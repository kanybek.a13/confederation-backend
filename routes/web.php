<?php

use App\Http\Controllers\Admin\AthleteController;
use App\Http\Controllers\Admin\CoachController;
use App\Http\Controllers\Admin\ExecutiveCommitteeController;
use App\Http\Controllers\Admin\HistoryController;
use App\Http\Controllers\Admin\MediaController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\PhotoController;
use App\Http\Controllers\Admin\ResultController;
use App\Http\Controllers\Admin\SectionController;
use App\Http\Controllers\Admin\TeamController;
use App\Http\Controllers\ConfederationController;
use App\Http\Controllers\FederationController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', [HomeController::class, 'start']);
Route::get('/admin-panel/login', [HomeController::class, 'adminLogin'])->name('admin.login');

Route::group( ['middleware' => 'auth', 'verified', 'admin.access'], function() {
    Route::get('/admin-panel', [HomeController::class, 'adminPanel']);

    // Manage Federation
    Route::get('/admin-panel/federations', [\App\Http\Controllers\Admin\FederationController::class, 'index']);
    Route::get('/admin-panel/federation/new', [\App\Http\Controllers\Admin\FederationController::class, 'create']);
    Route::get('/admin-panel/federation/{federation}', [\App\Http\Controllers\Admin\FederationController::class, 'show']);
    Route::get('/admin-panel/federation/{federation}/edit', [\App\Http\Controllers\Admin\FederationController::class, 'edit']);
    Route::delete('/admin-panel/federation/{federation}/delete', [\App\Http\Controllers\Admin\FederationController::class, 'destroy'])->name('admin.federation.delete');
    Route::post('/admin-panel/federation', [\App\Http\Controllers\Admin\FederationController::class, 'store']);
    Route::put('/admin-panel/federation/{federation}', [\App\Http\Controllers\Admin\FederationController::class, 'update']);
    Route::put('/admin-panel/federation/{federation}/update-logo', [\App\Http\Controllers\Admin\FederationController::class, 'updateLogo']);
    Route::put('/admin-panel/federation/{federation}/update-image', [\App\Http\Controllers\Admin\FederationController::class, 'updateImage']);

    // Manage News
    Route::get('/admin-panel/{federation}/news', [NewsController::class, 'index'])->name('admin.news');
    Route::get('/admin-panel/{federation}/news/create', [NewsController::class, 'create'])->name('admin.news.create');
    Route::get('/admin-panel/{federation}/news/{news}', [NewsController::class, 'show'])->name('admin.news.show');
    Route::get('/admin-panel/{federation}/news/{news}/edit', [NewsController::class, 'edit'])->name('admin.news.edit');
    Route::post('/admin-panel/{federation}/news/', [NewsController::class, 'store'])->name('admin.news.store');
    Route::put('/admin-panel/{federation}/news/{news}/update', [NewsController::class, 'update'])->name('admin.news.update');
    Route::delete('/admin-panel/{federation}/news/{news}/delete', [NewsController::class, 'destroy'])->name('admin.news.delete');

    // Manage Media
    Route::get('/admin-panel/{federation}/medias', [MediaController::class, 'index'])->name('admin.medias');
    Route::post('/admin-panel/{federation}/media', [MediaController::class, 'store'])->name('admin.media.store');
    Route::get('/admin-panel/{federation}/media/create/{type}', [MediaController::class, 'create'])->name('admin.media.create');
    Route::get('/admin-panel/{federation}/media/{media}/edit', [MediaController::class, 'edit'])->name('admin.media.edit');


    Route::get('/admin-panel/{federation}/media/{media}', [MediaController::class, 'show'])->name('admin.media.show');

    Route::get('/admin-panel/{federation}/media/{media}/edit', [MediaController::class, 'edit'])->name('admin.media.edit');
    Route::put('/admin-panel/{federation}/media/{media}/update', [MediaController::class, 'update'])->name('admin.media.update');
    Route::delete('/admin-panel/{federation}/media/{media}/delete', [MediaController::class, 'destroy'])->name('admin.media.delete');
    Route::delete('/admin-panel/{federation}/media/{media}/photo/{photo}/delete', [MediaController::class, 'destroyPhoto'])->name('admin.media.photo.delete');

    // Manage Media Photos
    Route::get('/admin-panel/{federation}/media-photos', [PhotoController::class, 'index'])->name('admin.media-photos');
    Route::get('/admin-panel/{federation}/media-photo/', [PhotoController::class, 'create'])->name('admin.media-photo.create');
    Route::get('/admin-panel/{federation}/media-photo/{photo}', [PhotoController::class, 'show'])->name('admin.media-photo.show');
    Route::get('/admin-panel/{federation}/media-photo/{photo}/edit', [PhotoController::class, 'edit'])->name('admin.media-photo.edit');
    Route::post('/admin-panel/{federation}/media-photo/', [PhotoController::class, 'store'])->name('admin.media-photo.store');
    Route::put('/admin-panel/{federation}/media-photo/{photo}/update', [PhotoController::class, 'update'])->name('admin.media-photo.update');
    Route::delete('/admin-panel/{federation}/media-photo/{photo}/delete', [PhotoController::class, 'destroy'])->name('admin.media-photo.delete');

    // Manage Teams
    Route::get('/admin-panel/{federation}/teams', [TeamController::class, 'index'])->name('admin.teams');
    Route::get('/admin-panel/{federation}/team/create', [TeamController::class, 'create'])->name('admin.team.create');
    Route::get('/admin-panel/{federation}/team/{team}', [TeamController::class, 'show'])->name('admin.team.show');
    Route::get('/admin-panel/{federation}/team/{team}/edit', [TeamController::class, 'edit'])->name('admin.team.edit');
    Route::post('/admin-panel/{federation}/team/store', [TeamController::class, 'store'])->name('admin.team.store');
    Route::put('/admin-panel/{federation}/team/{team}/update', [TeamController::class, 'update'])->name('admin.team.update');
    Route::delete('/admin-panel/{federation}/team/{team}/delete', [TeamController::class, 'destroy'])->name('admin.team.delete');

    // Manage Team Athlete
    Route::post('/admin-panel/{federation}/team/{team}/athlete/invite', [TeamController::class, 'inviteAthlete'])->name('admin.team.athlete.invite');
    Route::post('/admin-panel/{federation}/team/{team}/athlete/{athlete}/delete', [TeamController::class, 'deleteAthlete'])->name('admin.team.athlete.delete');

    // Manage Athletes
    Route::get('/admin-panel/{federation}/athletes', [AthleteController::class, 'index'])->name('admin.athletes');
    Route::get('/admin-panel/{federation}/athlete/create', [AthleteController::class, 'create'])->name('admin.athlete.create');
    Route::get('/admin-panel/{federation}/athlete/{athlete}', [AthleteController::class, 'show'])->name('admin.athlete.show');
    Route::get('/admin-panel/{federation}/athlete/{athlete}/edit', [AthleteController::class, 'edit'])->name('admin.athlete.edit');
    Route::post('/admin-panel/{federation}/athlete/store', [AthleteController::class, 'store'])->name('admin.athlete.store');
    Route::put('/admin-panel/{federation}/athlete/{athlete}/update', [AthleteController::class, 'update'])->name('admin.athlete.update');
    Route::delete('/admin-panel/{federation}/athlete/{athlete}/delete', [AthleteController::class, 'destroy'])->name('admin.athlete.delete');

    // Manage Coaches
    Route::get('/admin-panel/{federation}/coaches', [CoachController::class, 'index'])->name('admin.coaches');
    Route::get('/admin-panel/{federation}/coach/create', [CoachController::class, 'create'])->name('admin.coach.create');
    Route::get('/admin-panel/{federation}/coach/{coach}', [CoachController::class, 'show'])->name('admin.coach.show');
    Route::get('/admin-panel/{federation}/coach/{coach}/edit', [CoachController::class, 'edit'])->name('admin.coach.edit');
    Route::post('/admin-panel/{federation}/coach/', [CoachController::class, 'store'])->name('admin.coach.store');
    Route::put('/admin-panel/{federation}/coach/{coach}/update', [CoachController::class, 'update'])->name('admin.coach.update');
    Route::delete('/admin-panel/{federation}/coach/{coach}/delete', [CoachController::class, 'destroy'])->name('admin.coach.delete');

    // Manage Sections
    Route::get('/admin-panel/{federation}/sections', [SectionController::class, 'index'])->name('admin.sections');
    Route::get('/admin-panel/{federation}/section/create', [SectionController::class, 'create'])->name('admin.section.create');
    Route::get('/admin-panel/{federation}/section/{section}', [SectionController::class, 'show'])->name('admin.section.show');
    Route::get('/admin-panel/{federation}/section/{section}/edit', [SectionController::class, 'edit'])->name('admin.section.edit');
    Route::post('/admin-panel/{federation}/section/', [SectionController::class, 'store'])->name('admin.section.store');
    Route::put('/admin-panel/{federation}/section/{section}/update', [SectionController::class, 'update'])->name('admin.section.update');
    Route::delete('/admin-panel/{federation}/section/{section}/delete', [SectionController::class, 'destroy'])->name('admin.section.delete');

    # Manage Results
    Route::get('/admin-panel/{federation}/results', [ResultController::class, 'index'])->name('admin.results');
    Route::get('/admin-panel/{federation}/result/create', [ResultController::class, 'create'])->name('admin.result.create');
    Route::get('/admin-panel/{federation}/result/{result}', [ResultController::class, 'show'])->name('admin.result.show');
    Route::get('/admin-panel/{federation}/result/{result}/edit', [ResultController::class, 'edit'])->name('admin.result.edit');
    Route::post('/admin-panel/{federation}/result/', [ResultController::class, 'store'])->name('admin.result.store');
    Route::put('/admin-panel/{federation}/result/{result}/update', [ResultController::class, 'update'])->name('admin.result.update');
    Route::delete('/admin-panel/{federation}/result/{result}/delete', [ResultController::class, 'destroy'])->name('admin.result.delete');

    # Manage History
    Route::get('/admin-panel/{federation}/history', [HistoryController::class, 'show'])->name('admin.history');
    Route::get('/admin-panel/{federation}/history/create', [HistoryController::class, 'create'])->name('admin.history.create');
    Route::get('/admin-panel/{federation}/history/edit', [HistoryController::class, 'edit'])->name('admin.history.edit');
    Route::post('/admin-panel/{federation}/history/', [HistoryController::class, 'store'])->name('admin.history.store');
    Route::put('/admin-panel/{federation}/history/update', [HistoryController::class, 'update'])->name('admin.history.update');
    Route::delete('/admin-panel/{federation}/history/delete', [HistoryController::class, 'destroy'])->name('admin.history.delete');

    # Manage Executive Committee
    Route::get('/admin-panel/{federation}/executive-committees', [ExecutiveCommitteeController::class, 'index'])->name('admin.executive-committees');
    Route::get('/admin-panel/{federation}/executive-committee/create', [ExecutiveCommitteeController::class, 'create'])->name('admin.executive-committee.create');
    Route::get('/admin-panel/{federation}/executive-committee/{executiveCommittee}', [ExecutiveCommitteeController::class, 'show'])->name('admin.executive-committee.show');
    Route::get('/admin-panel/{federation}/executive-committee/{executiveCommittee}/edit', [ExecutiveCommitteeController::class, 'edit'])->name('admin.executive-committee.edit');
    Route::post('/admin-panel/{federation}/executive-committee/', [ExecutiveCommitteeController::class, 'store'])->name('admin.executive-committee.store');
    Route::put('/admin-panel/{federation}/executive-committee/{executiveCommittee}/update', [ExecutiveCommitteeController::class, 'update'])->name('admin.executive-committee.update');
    Route::delete('/admin-panel/{federation}/executive-committee/{executiveCommittee}/delete', [ExecutiveCommitteeController::class, 'destroy'])->name('admin.executive-committee.delete');

});

// App View Federations

Route::get('/{federation}', [FederationController::class, 'index']);
Route::get('/{federation}/news', [FederationController::class, 'news']);
Route::get('/{federation}/news/{news}', [FederationController::class, 'newsShow']);
Route::get('/{federation}/executive-committee', [FederationController::class, 'executiveCommunity']);
Route::get('/{federation}/apparatus', [FederationController::class, 'apparatus']);
Route::get('/{federation}/history', [FederationController::class, 'history']);
Route::get('/{federation}/media', [FederationController::class, 'media']);
Route::get('/{federation}/media/{media}', [FederationController::class, 'mediaShow']);
Route::get('/{federation}/video', [FederationController::class, 'video']);
Route::get('/{federation}/photo', [FederationController::class, 'photo']);
Route::get('/{federation}/goals', [FederationController::class, 'goals']);
Route::get('/{federation}/documents', [FederationController::class, 'documents']);
Route::get('/{federation}/article', [FederationController::class, 'article']);
Route::get('/{federation}/coaches', [FederationController::class, 'coaches']);
Route::get('/{federation}/coach/{coach}', [FederationController::class, 'coachShow']);
Route::get('/{federation}/interview', [FederationController::class, 'interview']);
Route::get('/{federation}/massmedia', [FederationController::class, 'massmedia']);
Route::get('/{federation}/events', [FederationController::class, 'events']);
Route::get('/{federation}/event/{event}', [FederationController::class, 'eventShow']);
Route::get('/{federation}/results', [FederationController::class, 'results']);
Route::get('/{federation}/teams', [FederationController::class, 'teams']);
Route::get('/{federation}/athlete/{athlete}', [FederationController::class, 'athlete']);
Route::get('/{federation}/sections', [FederationController::class, 'sections']);
Route::get('/{federation}/section/{section}', [FederationController::class, 'sectionShow']);
Route::get('/{federation}/structure', [FederationController::class, 'structure']);
Route::get('/{federation}/search', [FederationController::class, 'searchResult']);


Route::get('/partners-section', function () {
    return view('partners-section');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'home'])->name('home');
Route::get('/welcome', function () {
    return view('welcome');
});
