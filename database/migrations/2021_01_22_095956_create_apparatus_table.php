<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApparatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apparatuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullname');
            $table->string('position');
            $table->string('position_notes');
            $table->string('email');
            $table->string('phone');
            $table->string('image');
            $table->unsignedBigInteger('federation_id');

            $table->foreign('federation_id')
                ->references('id')
                ->on('federations')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apparatus');
    }
}
