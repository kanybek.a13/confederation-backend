<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coaches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullname');
            $table->string('position');
            $table->string('image');
            $table->unsignedBigInteger('federation_id');

            $table->foreign('federation_id')
                ->references('id')
                ->on('federations')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('team_coach', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('team_id');
            $table->unsignedBigInteger('coach_id');

            $table->foreign('team_id')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade');

            $table->foreign('coach_id')
                ->references('id')
                ->on('coaches')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coaches');
    }
}
