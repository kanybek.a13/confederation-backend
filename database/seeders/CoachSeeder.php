<?php

namespace Database\Seeders;

use App\Models\Coach;
use Illuminate\Database\Seeder;

class CoachSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<5; $i++){
            Coach::create([
                'fullname' => 'Тренер бокса',
                'position' => 'Тренер сборной Казахстана',
                'federation_id' => 1,
                'image' => '/assets/img/empty-human.jpg',
            ]);
        }

        for ($i=5; $i<8; $i++){
            Coach::create([
                'fullname' => 'Тренер борьбы',
                'position' => 'Тренер сборной Казахстана',
                'federation_id' => 2,
                'image' => '/assets/img/empty-human.jpg',
            ]);
        }

        for ($i=8; $i<12; $i++){
            Coach::create([
                'fullname' => 'Тренер дзюдо',
                'position' => 'Тренер сборной Казахстана',
                'federation_id' => 3,
                'image' => '/assets/img/empty-human.jpg',
            ]);
        }

        for ($i=12; $i<15; $i++){
            Coach::create([
                'fullname' => 'Тренер Таеквондо',
                'position' => 'Тренер сборной Казахстана',
                'federation_id' => 4,
                'image' => '/assets/img/empty-human.jpg',
            ]);
        }

        for ($i=16; $i<19; $i++){
            Coach::create([
                'fullname' => 'Тренер Тяжелой атлетики',
                'position' => 'Тренер сборной Казахстана',
                'federation_id' => 5,
                'image' => '/assets/img/empty-human.jpg',
            ]);
        }

        for ($i=20; $i<24; $i++){
            Coach::create([
                'fullname' => 'Тренер Каратэ',
                'position' => 'Тренер сборной Казахстана',
                'federation_id' => 6,
                'image' => '/assets/img/empty-human.jpg',
            ]);
        }

        for ($i=24; $i<26; $i++){
            Coach::create([
                'fullname' => 'Тренер Қазақ күресі',
                'position' => 'Тренер сборной Казахстана',
                'federation_id' => 7,
                'image' => '/assets/img/empty-human.jpg',
            ]);
        }


    }
}
