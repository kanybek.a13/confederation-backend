<?php

namespace Database\Seeders;

use App\Models\Photo;
use App\Models\TeamMember;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            FederationSeeder::class,
            ExecutiveCommitteeSeeder::class,
            ApparatusSeeder::class,
            PartnerSeeder::class,
            HistorySeeder::class,
            PartnerSeeder::class,
            MediaSeeder::class,
            PhotoSeeder::class,
            VideoSeeder::class,
            NewsSeeder::class,
            CoachSeeder::class,
            AthleteSeeder::class,
            TeamSeeder::class,
            AthleteSeeder::class,
            CitySeeder::class,
            SectionSeeder::class,
            EventSeeder::class,

        ]);
    }
}
