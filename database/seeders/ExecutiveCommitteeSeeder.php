<?php

namespace Database\Seeders;

use App\Models\ExecutiveCommittee;
use Illuminate\Database\Seeder;

class ExecutiveCommitteeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ExecutiveCommittee::create([
            'fullname' => 'Абулгазин Данияр Рустемович',
            'federation_id' => 1,
            'position' => 'Председатель Конфедерации,
Председатель исполнительного комитета',
            'image' => '/assets/img/executive-committee/1.jpg',
        ]);

        ExecutiveCommittee::create([
            'fullname' => 'Бектенов Бекжан Мухтасифович',
            'federation_id' => 1,
            'position' => 'Исполнительный директор ОЮЛ в ФА "Казахстанская федерация бокса",
Член исполнительного комитета',
            'image' => '/assets/img/executive-committee/2.jpg',
        ]);

        ExecutiveCommittee::create([
            'fullname' => 'Мынбаев Сауат Мухаметбаевич',
            'federation_id' => 1,
            'position' => 'Президент РОО «Федерация греко-римской, вольной и женской борьбы»,
Вице-председатель Конфедерации, Член исполнительного комитета',
            'image' => '/assets/img/executive-committee/3.jpg',
        ]);

        ExecutiveCommittee::create([
            'fullname' => 'Ким Вячеслав Константинович',
            'federation_id' => 1,
            'position' => 'Президент РОО «Федерация Таеквондо (WTF) Республики Казахстан»,
Вице-председатель Конфедерации, Член исполнительного комитета',
            'image' => '/assets/img/executive-committee/4.jpg',
        ]);

        ExecutiveCommittee::create([
            'fullname' => 'Тусупбеков Жанат Рашидович',
            'federation_id' => 1,
            'position' => 'Президент ОО «Федерация тяжелой атлетики» (республиканское),
Вице-председатель Конфедерации, Член исполнительного комитета',
            'image' => '/assets/img/executive-committee/5.jpg',
        ]);
    }
}
