<?php

namespace Database\Seeders;

use App\Models\Federation;
use Illuminate\Database\Seeder;

class FederationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Federation::create([
            'name' => 'Федерация бокса',
            'sport' => 'Бокс',
            'address' => 'Республика Казахстан, г. Астана, пр.Туран, 18, 9-й этаж',
            'fax' => '+7 (7172) 79-90-15',
            'email' => 'reception@kfb.kz',
            'phone' => '+7 (7172) 57-00-10',
            'logo' => '/assets/img/sports/box.png',
            'image' => '/assets/img/federations/box.jpg',
            'site' => 'box',
            'website' => 'http://kfb.kz/',
        ]);

        Federation::create([
            'name' => 'Федерация греко-римской, вольной и женской борьбы',
            'sport' => 'Борьба',
            'address' => 'Республика Казахстан, г. Астана, пр.Туран, 18, 9-й этаж',
            'fax' => '+7 (7172) 79-90-15',
            'email' => 'reception@kfb.kz',
            'phone' => '+7 (7172) 57-00-10',
            'logo' => '/assets/img/sports/wrestling.png',
            'image' => '/assets/img/federations/wrestling.jpg',
            'site' => 'wrestling',
            'website' => 'http://wrestling.kz/',
        ]);

        Federation::create([
            'name' => 'Федерация дзюдо',
            'sport' => 'Дзюдо',
            'address' => 'Республика Казахстан, г. Астана, пр.Туран, 18, 9-й этаж',
            'fax' => '+7 (7172) 79-90-15',
            'email' => 'reception@kfb.kz',
            'phone' => '+7 (7172) 57-00-10',
            'logo' => '/assets/img/sports/judo.png',
            'image' => '/assets/img/federations/judo.jpg',
            'site' => 'judo',
            'website' => 'http://fdk.kz/',
        ]);

        Federation::create([
            'name' => 'Федерация таеквондо',
            'sport' => 'Таеквондо',
            'address' => 'Республика Казахстан, г. Астана, пр.Туран, 18, 9-й этаж',
            'fax' => '+7 (7172) 79-90-15',
            'email' => 'reception@kfb.kz',
            'phone' => '+7 (7172) 57-00-10',
            'logo' => '/assets/img/sports/taekwondo.png',
            'image' => '/assets/img/federations/taekwondo.jpg',
            'site' => 'taekwondo',
            'website' => 'http://kaztkd.kz/',
        ]);

        Federation::create([
            'name' => 'Федерация тяжелой атлетики',
            'sport' => 'Тяжелая атлетика',
            'address' => 'Республика Казахстан, г. Астана, пр.Туран, 18, 9-й этаж',
            'fax' => '+7 (7172) 79-90-15',
            'email' => 'reception@kfb.kz',
            'phone' => '+7 (7172) 57-00-10',
            'logo' => '/assets/img/sports/weightlifting.png',
            'image' => '/assets/img/federations/weightlifting.jpg',
            'site' => 'weightlifting',
            'website' => 'http://wfrk.kz/',
        ]);

        Federation::create([
            'name' => 'Федерация каратэ',
            'sport' => 'Каратэ',
            'address' => 'Республика Казахстан, г. Астана, пр.Туран, 18, 9-й этаж',
            'fax' => '+7 (7172) 79-90-15',
            'email' => 'reception@kfb.kz',
            'phone' => '+7 (7172) 57-00-10',
            'logo' => '/assets/img/sports/karate.png',
            'image' => '/assets/img/federations/karate.jpg',
            'site' => 'karate',
            'website' => 'http://www.kazkarate.net/',
        ]);

        Federation::create([
            'name' => 'Федерация қазақ күресі',
            'sport' => 'Қазақ күресі',
            'address' => 'Республика Казахстан, г. Астана, пр.Туран, 18, 9-й этаж',
            'fax' => '+7 (7172) 79-90-15',
            'email' => 'reception@kfb.kz',
            'phone' => '+7 (7172) 57-00-10',
            'logo' => '/assets/img/sports/beldesu.png',
            'image' => '/assets/img/federations/beldesu.jpg',
            'site' => 'beldesu',
            'website' => 'http://www.beldesu.kz/',
        ]);

        Federation::create([
            'name' => 'Конфедерация',
            'sport' => 'Конфедерация',
            'address' => 'Республика Казахстан, г. Астана, пр.Туран, 18, 9-й этаж',
            'fax' => '+7 (7172) 79-90-15',
            'email' => 'reception@kfb.kz',
            'phone' => '+7 (7172) 57-00-10',
            'logo' => '/assets/img/logo.png',
            'image' => '',
            'site' => 'confederation',
            'website' => 'confederation',
        ]);
    }
}
