<?php

namespace Database\Seeders;

use App\Models\Section;
use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<22; $i++){
             Section::create([
                'name' => 'ДЮСШ №3',
                'phone' => '+7 (727) 233-56-66',
                'address' => 'Алматы, улица Наурызбай батыра',
                'image' => '/assets/img/sections/1.jpg',
                'city_id' => rand(1, 21),
                'federation_id' => rand(1, 7)
            ]);
        }
    }
}
