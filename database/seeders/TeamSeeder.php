<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $federations = [1, 3, 4, 5, 6];

        for ($i=0; $i<sizeof($federations); $i++){
            Team::create([
                'name' => 'МУЖСКОЙ СОСТАВ',
                'slug' => 'muzhskoi-sostav',
                'federation_id' => $federations[$i]
            ]);

            Team::create([
                'name' => 'ЖЕНСКИЙ СОСТАВ',
                'slug' => 'zhenskii-sostav',
                'federation_id' => $federations[$i]
            ]);

            Team::create([
                'name' => 'МУЖСКОЙ МОЛОДЕЖНЫЙ СОСТАВ',
                'slug' => 'muzhskoi-molodezhnii-sostav',
                'federation_id' => $federations[$i]
            ]);

            Team::create([
                'name' => 'ЖЕНСКИЙ МОЛОДЕЖНЫЙ СОСТАВ',
                'slug' => 'zhenskii-molodezhnii-sostav',
                'federation_id' => $federations[$i]
            ]);
        }

        //WRESTLING TEAMS
        Team::create([
            'name' => 'ГРЕКО-РИМСКАЯ',
            'slug' => 'greko-rimskaiya',
            'federation_id' => 2
        ]);

        Team::create([
            'name' => 'ВОЛЬНАЯ',
            'slug' => 'volnaiya',
            'federation_id' => 2
        ]);

        Team::create([
            'name' => 'ЖЕНСКАЯ',
            'slug' => 'zhenskaiya',
            'federation_id' => 2
        ]);

        //BELDESU TEAMS
        Team::create([
            'name' => 'МУЖСКОЙ СОСТАВ',
            'slug' => 'muzhskoi-sostav',
            'federation_id' => 7
        ]);

        Team::create([
            'name' => 'МУЖСКОЙ МОЛОДЕЖНЫЙ СОСТАВ',
            'slug' => 'muzhskoi-molodezhnii-sostav',
            'federation_id' => 7
        ]);
    }
}
