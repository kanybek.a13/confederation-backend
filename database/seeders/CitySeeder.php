<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities =['Жезказган', 'Каскелен', 'Актау', 'Актобе', 'Алматы', 'Кызылорда', 'Астана',
            'Атырау', 'Павлодар', 'Талдыкорган', 'Уральск', 'Усть-Каменогорск', 'Караганда',
            'Темиртау', 'Боровое', 'Кокшетау', 'Костанай', 'Петропавловск', 'Тараз', 'Туркестан', 'Шымкент'];

        foreach ($cities as $city){
            City::create([
                'name' => $city,
            ]);
        }
    }
}
