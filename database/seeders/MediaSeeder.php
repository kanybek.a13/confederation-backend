<?php

namespace Database\Seeders;

use App\Models\Media;
use Illuminate\Database\Seeder;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Media::create([
            'name' => 'test photo media',
            'type' => 'photo',
            'image' => '/assets/img/photos/1.jpg',
            'views' => '2',
            'federation_id' => 1,
        ]);

        Media::create([
            'name' => 'test video media',
            'type' => 'video',
            'image' => 'https://img.youtube.com/vi/eA_CWh_GF6Y/mqdefault.jpg',
            'views' => '2',
            'federation_id' => 1,
        ]);
    }
}
