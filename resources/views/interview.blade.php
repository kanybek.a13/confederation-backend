@extends('layouts.app')
@section('main')

<section class="news">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Интервью</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Интервью</span></li>
                </ul>
            </div>
            <div class="list row">
                <div class="col-sm-6">
                    <a href="#" class="d-flex align-items-center">
                        <img src="/assets/img/interview/1.jpg">
                        <div>
                            <h3>Выигравшая у казахстанки девушка из Вьетнама оказалась парнем</h3>
                            <div class="post-details">
                                <span class="date">3 сентября</span>
                                <span><i class="far fa-eye"></i> 1290</span>
                                <span><i class="fas fa-comments"></i> 12</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6">
                    <a href="#" class="d-flex align-items-center">
                        <img src="/assets/img/interview/2.jpg">
                        <div>
                            <h3>В Конфедерации спортивных единоборств
                                и силовых видов спорта назначен новый...</h3>
                            <div class="post-details">
                                <span class="date">3 сентября</span>
                                <span><i class="far fa-eye"></i> 1290</span>
                                <span><i class="fas fa-comments"></i> 12</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6">
                    <a href="#" class="d-flex align-items-center">
                        <img src="/assets/img/interview/1.jpg">
                        <div>
                            <h3>Выигравшая у казахстанки девушка из Вьетнама оказалась парнем</h3>
                            <div class="post-details">
                                <span class="date">3 сентября</span>
                                <span><i class="far fa-eye"></i> 1290</span>
                                <span><i class="fas fa-comments"></i> 12</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6">
                    <a href="#" class="d-flex align-items-center">
                        <img src="/assets/img/interview/2.jpg">
                        <div>
                            <h3>В Конфедерации спортивных единоборств
                                и силовых видов спорта назначен новый...</h3>
                            <div class="post-details">
                                <span class="date">3 сентября</span>
                                <span><i class="far fa-eye"></i> 1290</span>
                                <span><i class="fas fa-comments"></i> 12</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6">
                    <a href="#" class="d-flex align-items-center">
                        <img src="/assets/img/interview/1.jpg">
                        <div>
                            <h3>Выигравшая у казахстанки девушка из Вьетнама оказалась парнем</h3>
                            <div class="post-details">
                                <span class="date">3 сентября</span>
                                <span><i class="far fa-eye"></i> 1290</span>
                                <span><i class="fas fa-comments"></i> 12</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6">
                    <a href="#" class="d-flex align-items-center">
                        <img src="/assets/img/interview/2.jpg">
                        <div>
                            <h3>В Конфедерации спортивных единоборств
                                и силовых видов спорта назначен новый...</h3>
                            <div class="post-details">
                                <span class="date">3 сентября</span>
                                <span><i class="far fa-eye"></i> 1290</span>
                                <span><i class="fas fa-comments"></i> 12</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
