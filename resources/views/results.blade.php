@extends('layouts.app')
@section('main')
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Документы</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Документы</span></li>
                </ul>
            </div>

            @forelse($results as $result)
                <a href="{{$result->path}}" download="" class="card-regular" target="_blank">
                    <img class="icon" src="/assets/img/file.png">
                    <div>
                        <h3 class="title">{{$result->name}}</h3>
                        <p class="grey">{{$result->created_at}}</p>
                    </div>
                    <div class="d-flex align-items-center download-details">
                        <i class="flaticon-down"></i>
                    </div>
                </a>
            @empty
                No documents yet!
            @endforelse
        </div>
    </div>

@endsection
