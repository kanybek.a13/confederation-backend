@extends('layouts.app')
@section('main')

<section class="interview">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Сборные команды</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Состав сборной</span></li>
                </ul>
            </div>
            <div class="links">
                @forelse($teams as $team)
                    <li class="nav-item">
                        <a class="nav-link {{$team->id == request('team') ? 'active' : ''}}" href="/{{$currentFederation->site}}/teams?team_id={{$team->id}}" aria-controls="home" aria-selected="true">{{$team->name}}</a>
                    </li>
                @empty
                    <p>No teams yet!</p>
                @endforelse
            </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                @forelse($teamAthletes as $athlete)
                    <div class="col-lg-6">
                        <a href="/{{$currentFederation->site}}/athlete/{{$athlete->id}}" class="d-flex align-items-center">
                            <img src="{{$athlete->image}}">
                            <div>
                                <h3>{{$athlete->fullname}}</h3>
                                <p><span class="grey">Дата рождения: </span> {{$athlete->date_of_birth}}</p>
                                <p><span class="grey">Весовая категория:</span> {{$athlete->weight}}</p>
                                <p><span class="grey">Тренер: </span>
                                    @if($athlete->team[0]->coaches[0])
                                        {{$athlete->team[0]->coaches[0]->fullname}}
                                    @else
                                        No coaches yet!
                                    @endif
                                </p>
                            </div>
                        </a>
                    </div>
                @empty
                    <p>No athletes yet!</p>
                @endforelse
            </div>
            <br>
        </div>
    </div>
</section>




{{--<section>--}}
{{--    <div class="container">--}}
{{--        <div class="white-content">--}}
{{--            <div class="title-breadcrumbs">--}}
{{--                <h2>Состав сборной</h2>--}}
{{--                <ul class="breadcrumbs">--}}
{{--                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>--}}
{{--                    <li><span>Состав сборной</span></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--            <ul class="links" id="myTab" role="tablist">--}}
{{--                @forelse($teams as $team)--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link {{$team->id == request('team') ? 'active' : ''}}" href="/{{$currentFederation->site}}/teams?team_id={{$team->id}}" aria-controls="home" aria-selected="true">{{$team->name}}</a>--}}
{{--                    </li>--}}
{{--                @empty--}}
{{--                    <p>No teams yet!</p>--}}
{{--                @endforelse--}}
{{--            </ul>--}}
{{--            <div class="tab-content" id="myTabContent">--}}
{{--                <div class="container">--}}
{{--                    <div class="row">--}}
{{--                        @forelse($teamAthletes as $athlete)--}}
{{--                            <div class="col-lg-6 white-content" style="margin-bottom:2px;">--}}
{{--                                <a href="/{{$currentFederation->site}}/team/{{$team->id}}/athlete/{{$athlete->id}}" class="d-flex align-items-center">--}}
{{--                                    <div class="card-regular">--}}
{{--                                        <img src="{{$athlete->image}}" width="150" height="150">--}}
{{--                                        <div>--}}
{{--                                            <h3>{{$athlete->fullname}}</h3>--}}
{{--                                            <p><span class="grey">Дата рождения:</span> {{$athlete->date_of_birth}}</p>--}}
{{--                                            <p><span class="grey">Весовая категория:</span> {{$athlete->weight}}</p>--}}
{{--                                            <p><span class="grey">Тренер:</span>--}}
{{--                                                @if($athlete->team[0]->coaches[0])--}}
{{--                                                    {{$athlete->team[0]->coaches[0]->fullname}}--}}
{{--                                                @else--}}
{{--                                                    No coaches yet!--}}
{{--                                                @endif--}}
{{--                                            </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        @empty--}}
{{--                            <p>No athletes yet!</p>--}}
{{--                        @endforelse--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}

@endsection

