@extends('layouts.app')
@section('main')

    <article>
    <div class="container">

        <div class="white-content">
            <div class="title-block d-flex justify-content-between">
                <div>
                    <h1>Майра Бакашева: "Спортсмены стали более осознанными"</h1>
                    <div class="date">18 июня</div>
                </div>

                <div class="ya-share2" data-title="" data-description="" data-image="" data-services="facebook,vkontakte,pinterest,gplus"></div>
            </div>

            <div class="image">
                <a href="/assets/img/articles/1.jpg" data-fancybox=""><img src="/assets/img/articles/1.jpg"></a>
            </div>
            <div class="plain-text">
                <p>В минувший четверг на базе Всемирной Академии Бокса AIBA (Алматинская область) прошел антидопинговый семинар для мужской сборной РК по тяжелой атлетике. Организатором выступил Казахстанский Национальный антидопинговый центр.</p>
                <p><strong>Директор КазНАДЦ Майра Бакашева:</strong></p>
                <p>- У нас прошла очередная запланированная встреча – семинар для спортсменов и тренеров по тяжелой атлетике. Более трех часов откровенной продуктивной беседы. Ребята были очень вовлечены, отвечали на вопросы, сами спрашивали об интересующих их темах.</p>
                <p>Обсуждали насущные проблемы, говорили о новых стандартах, в том числе в сфере образования. Отдельное внимание было уделено спортивному питанию и последствиям, которые влечет за собой его неосторожное применение. Ведь такие, безопасные на первый взгляд, препараты, как БАДы, на деле могут привести к серьезным проблемам. Сейчас нами ведется биологический паспорт на каждого спортсмена, который позволяет выявлять нарушения по допингу путём регистрации отклонений от нормы, не прибегая к тестированию и идентификации отдельных запрещенных веществ.</p>
                <p>Говорили про все, абсолютно. Отмечу, что сознание спортсмена сегодня кардинально отличается от сознания, которое было у ребят, допустим, лет 5 назад. Все стали более осознанными, нет уже каких-то смешков, глупых вопросов, несерьезного отношения. Атлеты понимают, что в первую очередь эти встречи нужны им, - отметила Майра Бакашева.</p>
                <p>В скором времени подобный семинар пройдет для женской сборной по тяжелой атлетике.</p>
            </div>
            <div class="article-details d-flex justify-content-between align-items-center">
                <div class="ya-share2" data-title="" data-description="" data-image="" data-services="facebook,vkontakte,pinterest,gplus"></div>
                <div>
                    <span>152 просмотра</span>
                    <span>0 комментариев</span>
                </div>
            </div>
        </div>
    </div>
</article>

<section class="news">
    <div class="container">
        <div class="white-content">
            <h2>Читать также:</h2>
            <div class="list row">
                <div class="col-lg-6">
                    <a href="#" class="d-flex align-items-center">
                        <img src="/assets/img/articles/2.jpg">
                        <div>
                            <h3>Выигравшая у казахстанки девушка из Вьетнама оказалась парнем</h3>
                            <p class="meta">07 июня 2018 12:49</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6">
                    <a href="#" class="d-flex align-items-center">
                        <img src="/assets/img/articles/3.jpg">
                        <div>
                            <h3>В Конфедерации спортивных единоборств
                                и силовых видов спорта назначен новый...</h3>
                            <p class="meta">07 июня 2018 15:44</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6">
                    <a href="#" class="d-flex align-items-center">
                        <img src="/assets/img/articles/2.jpg">
                        <div>
                            <h3>Выигравшая у казахстанки девушка из Вьетнама оказалась парнем</h3>
                            <p class="meta">07 июня 2018 12:49</p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6">
                    <a href="#" class="d-flex align-items-center">
                        <img src="/assets/img/articles/3.jpg">
                        <div>
                            <h3>В Конфедерации спортивных единоборств
                                и силовых видов спорта назначен новый...</h3>
                            <p class="meta">07 июня 2018 15:44</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
