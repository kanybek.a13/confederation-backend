@extends('layouts.app')
@section('main')

    <section class="news">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Новсти</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Новости</span></li>
                </ul>
            </div>

            <div class="list row">

                @forelse($last_news as $news)
                    <div class="col-lg-6">
                        <a href="{{'/' . $currentFederation->site . $news->path()}}" class="d-flex align-items-center">
                            <img src="{{$news->files[0]->path}}">
                            <div>
                                <h3>{{$news->title}}</h3>
                                <p class="meta">{{$news->created_at}}</p>
                            </div>
                        </a>
                    </div>
                @empty
                    <p>No news yet!</p>
                @endforelse

            </div>

            <ul class="pagination">
                <li class="previous_page"><a {{$last_news->currentPage() === 1 ? 'disabled' : '' }} href="{{request()->getRequestUri()}}?page={{ $last_news->currentPage() - 1 }}"><i class="fas fa-long-arrow-alt-left"></i></a></li>

                @for($page = 1; $page <= $last_news->lastPage(); $page++)
                    <li><a {{ $page === $last_news->currentPage() ? 'class="active"' : '' }} href="/{{$currentFederation->site}}/news?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$last_news->currentPage() === $last_news->lastPage() ? 'disabled' : '' }} href="/news?page={{ $last_news->currentPage() + 1 }}"><i class="fas fa-long-arrow-alt-right"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<script>
    $('.news__main').on('init', function () {
        setTimeout(function () {
            $('.news__main .slick-track').height($('.news__main .slick-track').height());
        }, 100)
    }).slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        dots: true,
        arrows: false,
        draggable: false,
        swipe: false,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    swipe: true
                }
            }
        ]
    })
</script>

<script>
    var owl = $('.partners-carousel');
    owl.owlCarousel({
//        dots: true,
//        autoWidth:true,
        nav: false,
        margin: 30,
        responsive: {
            1600: {
                items: 4
            },
            1024: {
                items: 4,
                autoWidth:false
            },
            768: {
                items: 3,
                margin: 15,
                autoWidth:false
            },
            600: {
                items: 2,
                margin: 10,
                autoWidth:false
            },
            0: {
                items: 1,
                autoWidth:false
            }
        }
    });
    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY > 0) {
            $(this).trigger('next.owl');
        } else {
            $(this).trigger('prev.owl');
        }
        e.preventDefault();
    });
</script>

@endsection
