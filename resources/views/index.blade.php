@extends('layouts.app')
@section('main')

<section class="news">
    <div class="container">
        <div class="white-content">
            <div class="title-block d-flex justify-content-between">
                <h2>Новости</h2>
                <a href="/{{$currentFederation->site}}/news" class="button-secondary">Перейти ко всем новостям</a>
            </div>

            <div class="row">
                <div class="col-lg-7">
                    <div class="news__main">
                        <div class="item">
                            @if(count($last_news)>0)
                                <div class="image">
                                    <a href="{{ $last_news[0]->files[0]->path }}" data-fancybox=""><img src="{{ $last_news[0]->files[0]->path }}"></a>
                                </div>
                                <div class="desc">
                                    <h3><a href="/{{$currentFederation->site}}/news/{{ $last_news[0]->id }}">{{ $last_news[0]->title }}</a></h3>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="meta">{{ $last_news[0]->created_at }}</span>
                                        <a href="/{{$currentFederation->site}}/news/{{ $last_news[0]->id }}" class="button-primary pull-right">Подробнее</a>
                                    </div>
                                </div>
                            @else
                                <p>No news yet!</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="news__list typical-scroll" style="display: none">
                        @forelse($last_news as $news)
                            @if ($loop->first) @continue @endif

                            <a href="/{{$currentFederation->site}}/news/{{$news->id}}" class="item">
                                <h3>{{$news->title}}</h3>
                                <p class="meta">{{$news->created_at}}</p>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        setTimeout(function () {
            $('.news__list').height($('.news__main').outerHeight()).show();
        }, 500);
    });
</script>

{{--<section>--}}
{{--    <div class="container">--}}
{{--        <div class="next-game">--}}
{{--            <img src="/assets/img/games/1.png">--}}
{{--            <div class="flex">--}}
{{--                <img src="/assets/img/games/1.png">--}}
{{--                <div>--}}
{{--                    <h4>До Азиатских игр осталось:</h4>--}}
{{--                    <p><span><span id="days"></span> <span>дней</span></span><span><span id="hours"></span> <span>часов</span></span><span><span id="minutes"></span> <span>минут</span></span><span--}}
{{--                    ><span id="seconds"></span> <span>секунд</span></span></p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}

{{--<script>--}}
{{--    var countDownDate = new Date("Sep 5, 2018 15:37:25").getTime();--}}

{{--    var x = setInterval(function () {--}}

{{--        var now = new Date().getTime();--}}

{{--        var distance = countDownDate - now;--}}

{{--        var days = Math.floor(distance / (1000 * 60 * 60 * 24));--}}
{{--        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));--}}
{{--        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));--}}
{{--        var seconds = Math.floor((distance % (1000 * 60)) / 1000);--}}

{{--        document.getElementById("days").innerHTML = days;--}}
{{--        document.getElementById("hours").innerHTML = hours;--}}
{{--        document.getElementById("minutes").innerHTML = minutes;--}}
{{--        document.getElementById("seconds").innerHTML = '<span>' + seconds + '</span>';--}}

{{--        if (distance < 0) {--}}
{{--            clearInterval(x);--}}
{{--            document.getElementById("demo").innerHTML = "Идет";--}}
{{--        }--}}
{{--    }, 1000);--}}
{{--</script>--}}

<section class="federations" id="federations">

    <div class="container">
        <div class="title-block d-flex justify-content-between">
            <h2>Федерации</h2>
            <div class="title-block__carousel-controls d-flex">
                <span class="fas fa-angle-left" onclick="$('.federations-carousel').trigger('prev.owl.carousel', [500]);"></span>
                <span class="fas fa-angle-right" onclick="$('.federations-carousel').trigger('next.owl.carousel', [500]);"></span>
            </div>
        </div>
        <div class="federations-carousel owl-carousel">
            @forelse($allFederations as $federation)
                @if($federation->name!=='Конфедерация')
                    <a href="{{$federation->site}}" class="item">
                        <div class="item-top">
                            <img class="man" src="{{$federation->image}}">
                            <div class="logo">
                                <div><img src="{{$federation->logo}}"></div>
                            </div>
                        </div>
                        <h3>{{$federation->name}}</h3>
                    </a>
                @endif
            @endforeach
        </div>
    </div>
</section>

<script>
    var owl = $('.federations-carousel');
    owl.owlCarousel({
        nav: false,
        margin: 30,
        smartSpeed: 800,
        responsive: {
            1600: {
                items: 4
            },
            1024: {
                items: 4
            },
            768: {
                items: 3,
                margin: 15,
                autoWidth: false
            },
            600: {
                items: 2,
                margin: 10,
                autoWidth: false
            },
            0: {
                items: 1,
                autoWidth: false
            }
        }
    });
    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY > 0) {
            $(this).trigger('next.owl');
        } else {
            $(this).trigger('prev.owl');
        }
        e.preventDefault();
    });
</script>

<section class="news competitions">
    <div class="container">

        <div class="white-content">
            <h2>Кубок конфедераций 2018</h2>
            <div class="news__main">
                <div class="item d-flex">
                    <div class="image">
                        <div class="image__item">
                            <img src="/assets/img/competitions/1.png">
                        </div>
                        <div class="image__item">
                            <img src="/assets/img/competitions/2.png">
                        </div>
                        <div class="image__item">
                            <img src="/assets/img/competitions/3.png">
                        </div>
                        <div class="image__item">
                            <img src="/assets/img/competitions/4.png">
                        </div>
                    </div>
                    <div class="desc">
                        <!--<h3><a href="#">КУБОК КОНФЕДЕРАЦИЙ <span>2018</span></a></h3>-->
                        <p class="big">Общий призовой фонд 145 млн тенге!</p>
                        <p>Кубок Конфедерации - уникальный турнир по по боксу, греко-римской, вольной борьбе, казах курес, дзюдо и таеквондо и тяжелой атлетике, которые проходят на всех площадках одновременно. За выход в финальный турнир борются команды областей и городов Астана, Алматы и Шымкент. Отборочные турниры сезона 2018 состоятся в октябре.</p>
                        <p>Победители групп получат путевки на финальный этап, который состоится декабре в городе Алматы.</p>
                        <p class="gold">Поддержи свою команду!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('.competitions .image').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplayspeed: 2000,
        fade: true
    })
</script>

@endsection
