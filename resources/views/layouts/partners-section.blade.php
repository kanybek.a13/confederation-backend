<section class="partners">
    <div class="container">
        <div class="white-content">
            <h2>Спонсоры и партнеры</h2>
            <div class="partners-carousel owl-carousel">
                @forelse($partners as $partner)
                    <a href="{{$partner->site}}">
                        <img src="{{$partner->image}}">
                    </a>
                @empty
                    <p>No partners yet!</p>
                @endforelse
            </div>
        </div>
    </div>
</section>

<script>
    var owl = $('.partners-carousel');
    owl.owlCarousel({
//        dots: true,
//        autoWidth:true,
        nav: false,
        margin: 30,
        responsive: {
            1600: {
                items: 4
            },
            1024: {
                items: 4,
                autoWidth:false
            },
            768: {
                items: 3,
                margin: 15,
                autoWidth:false
            },
            600: {
                items: 2,
                margin: 10,
                autoWidth:false
            },
            0: {
                items: 1,
                autoWidth:false
            }
        }
    });
    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY > 0) {
            $(this).trigger('next.owl');
        } else {
            $(this).trigger('prev.owl');
        }
        e.preventDefault();
    });
</script>

<!--<script>-->
<!--$('.partners__carousel').slick({-->
<!--slidesToShow: 1,-->
<!--slidesToScroll: 1,-->
<!--arrows: false,-->
<!--autoplay: true,-->
<!--autoplayspeed: 2000-->
<!--})-->
<!--</script>-->
