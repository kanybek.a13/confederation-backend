
</div>

<footer>
    <div class="footer__bottom">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="left-block">
                    <div class="copyright">&copy; 2018 КОНФЕДЕРАЦИЯ СПОРТИВНЫХ ЕДИНОБОРСТВ И СИЛОВЫХ ВИДОВ СПОРТА</div>
                    <a data-fancybox data-src="#form" href="javascript:;">Напишите нам</a>
                    <a href="#">Рекламодателям</a>
                </div>
                <a href="http://panama.kz" target="_blank" class="developer"><img
                        src="/assets/img/panama-white.png"></a>
            </div>
        </div>
    </div>
</footer>

<!--modals.BEGIN-->
    @include('layouts.modal')
<!--END.modals-->

<script src="/assets/libs/object-fit-images/dist/ofi.browser.js"></script>
<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="//yastatic.net/share2/share.js"></script>
</body>
</html>
