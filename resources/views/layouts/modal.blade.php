<div style="display: none;" id="form">
    <h3 class="title">Оставьте заявку</h3>
    <div class="form">
        <div class="preloader"></div>
        <form>
            <div class="input-group">
                <label>Ваше имя *</label>
                <input type="text" name="name" required>
            </div>
            <div class="input-group">
                <label>Ваш телефон *</label>
                <input type="tel" onfocus="$(this).inputmask('+7 999 999 99 99')" name="phone" required>
            </div>
            <div class="input-group">
                <label>E-Mail *</label>
                <input type="email" name="email" required>
            </div>
            <div class="input-group">
                <label>Ваше обращение *</label>
                <textarea name="message" required></textarea>
            </div>
            <div class="input-group">
                <input id="agreement" type="checkbox" onchange="ifChecked($(this))">
                <label for="agreement">Даю согласие на обработку предоставленных мной
                    личных данных</label>
            </div>
            <a href="javascript:void(0)" class="button-primary" onclick="validate($(this).closest('form'))" disabled>Отправить сообщение</a>
        </form>
    </div>
</div>
<div style="display: none;" id="success">
    <h3 class="message">Ваша заявка успешно отправлена!</h3>
</div>
