<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Title</title>

    <link rel="stylesheet" href="/assets/css/style.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/assets/libs/bootstrap/bootstrap.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/assets/libs/slick-carousel/slick/slick.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/assets/libs/fancybox/dist/jquery.fancybox.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/assets/libs/chosen/chosen.css">
    <link rel="stylesheet" href="/assets/libs/owl-carousel/owl.carousel.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/assets/libs/mCustomScrollbar/jquery.mCustomScrollbar.css">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400,500,600,700&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <script src="/assets/libs/jquery/dist/jquery.js"></script>
    <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
    <script src="/assets/libs/bootstrap/bootstrap.min.js"></script>
    <script src="/assets/libs/slick-carousel/slick/slick.js"></script>
    <script src="/assets/libs/fancybox/dist/jquery.fancybox.min.js"></script>
    <script src="/assets/libs/chosen/chosen.jquery.js"></script>
    <script src="/assets/libs/owl-carousel/owl.carousel.min.js"></script>
    <script src="/assets/libs/owl-carousel/jquery.mousewheel.min.js"></script>
    <script src="/assets/libs/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/assets/libs/maskedinput/maskedinput.js"></script>
    <script src="/assets/libs/object-fit-images/dist/ofi.browser.js"></script>

    <script src="/assets/js/scripts.js"></script>
    <script src="/assets/js/send.js"></script>
</head>

<body>

<header>
    <div class="header__top">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <div class="left-block">
                    <a href="#">Контакты</a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                </div>
                <div class="right-block">
                    <div class="lang">
                        <a href="#" class="active">РУС</a>
                        <a href="#">ҚАЗ</a>
                        <a href="#" style="border: none">ENG</a>
                    </div>
                    <div class="menu-btn">
                        <div class="nav-icon" onclick="$(this).toggleClass('opened'); $('.menu').toggleClass('active')">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="container">
            <nav>
                <ul class="mob-menu">
                    <li><a href="#">Контакты</a></li>
                </ul>
                <div class="mob-langs d-lg-none">
                    <a href="#" class="active">РУС</a> | <a href="#">ҚАЗ</a> | <a href="#">ENG</a>
                </div>
            </nav>
        </div>
    </div>
</header>

@yield('content')
