<!DOCTYPE html>
<html lang="en">

@include('layouts.head')

<body>
<header>
    <div class="header__bottom">
        <div class="container">
            <div class="d-flex flex-wrap flex-lg-nowrap justify-content-between">
                <a href="/{{$currentFederation->site}}" class="header__logo">
                    <img src="{{$currentFederation->logo}}">
                    <span>{{$currentFederation->name}}</span>
                </a>
                <div class="header__sports">
                    @forelse($allFederations as $federation)
                        @if($currentFederation->site === $federation->site) @continue @endif
                        <a href="/{{$federation->site}}"><img src="{{$federation->logo}}"></a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="header__top">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <div class="left-block">
                    <a href="#">Контакты</a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="http://olympic.kz" target="_blank" class="mob-hidden">#TEAMKZ</a>
                </div>
                <div class="right-block">
                    <div class="lang">
                        <a href="#" class="active">РУС</a>
                        <a href="#">ҚАЗ</a>
                        <a href="#">ENG</a>
                    </div>
                    <a href="javascript:void(0)" onclick="$('.header__search').fadeIn()">
                        <i class="fas fa-search"></i>
                    </a>
                    <div class="menu-btn">
                        <div class="nav-icon" onclick="$(this).toggleClass('opened'); $('.menu').toggleClass('active')">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>

            <form class="header__search">
                <div class="d-flex">
                    <input type="text" name="search">
                    <button type="submit"><i class="fas fa-search"></i></button>
                    <span onclick="$('.header__search').fadeOut()">&#10006;</span>
                </div>
            </form>
        </div>
    </div>
    <div class="menu">
        <div class="container">
            <nav>
                <ul class="mob-menu">
                    <li class="dropdown">
                        <a href="javascript:void(0)">Федерации</a>
                        <div class="dropdown-menu">
                            <ul class="container">
                                @forelse($allFederations as $federation)
                                    <li><a href="/{{$federation->site}}">{{$federation->sport}}</a></li>
                                @empty
                                    <p>No federations yet!</p>
                                @endforelse
                            </ul>
                        </div>
                    </li>
                </ul>
                <ul>
                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/goals">О Федерации</a>
                        <div class="dropdown-menu">
                            <ul class="container">
                                <li><a href="/{{$currentFederation->site}}/executive-committee">Руководство</a></li>
                                <li><a href="/{{$currentFederation->site}}/structure">Структура Федерации</a></li>
                                <li><a href="/{{$currentFederation->site}}/history">История Конфедерации</a></li>
                                <li><a href="/{{$currentFederation->site}}/documents">Документы</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/news" class="scrollto">Новости</a>
                    </li>
                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/media">Медиа</a>
{{--                        <div class="dropdown-menu">--}}
{{--                            <ul class="container">--}}
{{--                                <li><a href="/{{$currentFederation->site}}/photo">Фото</a></li>--}}
{{--                                <li><a href="/{{$currentFederation->site}}/video">Видео</a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
                    </li>
{{--                        <li class="dropdown">--}}
{{--                            <a href="#federations" class="scrollto">Федерации</a>--}}
{{--                            <div class="dropdown-menu">--}}
{{--                                <ul class="container">--}}
{{--                                    @forelse($allFederations as $federation)--}}
{{--                                        <li><a href="/{{$federation->site}}">{{$federation->sport}}</a></li>--}}
{{--                                    @empty--}}
{{--                                        <p>No federations yet!</p>--}}
{{--                                    @endforelse--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </li>--}}
                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/events" class="scrollto">Календарь событий</a>
                    </li>
                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/results" class="scrollto">Результаты</a>
                    </li>
                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/teams" class="scrollto">Сборные команды</a>
                    </li>
                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/coaches" class="scrollto">Тренерский состав</a>
                    </li>
                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/sections" class="scrollto">Секции</a>
                    </li>
                </ul>
{{--                <ul class="cups-menu">--}}
{{--                    <li><a href="#" class="active">Кубок 2018</a></li>--}}
{{--                    <li><a href="#">Кубок 2017</a></li>--}}
{{--                    <li><a href="#">Кубок 2016</a></li>--}}
{{--                </ul>--}}
                <ul class="mob-menu">
                    <li><a href="#">Контакты</a></li>
                </ul>
                <div class="mob-langs d-lg-none">
                    <a href="#" class="active">РУС</a> | <a href="#">ҚАЗ</a> | <a href="#">ENG</a>
                </div>
            </nav>
        </div>
    </div>
</header>

<div class="main-wrapper">

<!--Main part-->
    @yield('content')
<!---->
