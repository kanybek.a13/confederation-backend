@extends('layouts.app')
@section('main')
    <section class="news">
        <div class="container">
            <div class="white-content">
                <div class="title-breadcrumbs">
                    <h2>Бозбаев Ислам</h2>
                    <ul class="breadcrumbs">
                        <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                        <li><a href="/{{$currentFederation->site}}/teams">Сборные команды</a></li>
                        <li><span>{{$athlete->fullname}}</span></li>
                    </ul>
                </div>

                <div class="news__main">
                    <div class="item d-flex">
                        <div class="image">
                            <img src="{{$athlete->image}}" width="150" height="250">
                        </div>
                        <div class="desc">
                            <div class="annotation">
                                <p><span class="grey">Дата рождения: </span> {{$athlete->date_of_birth}}</p>
                                <p><span class="grey">Место рождения: </span> {{$athlete->place_of_birth}}</p>
                                <p><span class="grey">Весовая категория:</span> {{$athlete->weight}}</p>
                                <p><span class="grey">Тренер: </span>
                                    @if($athlete->team[0]->coaches[0])
                                        {{$athlete->team[0]->coaches[0]->fullname}}
                                    @else
                                        No coaches yet!
                                    @endif
                                </p>
                                <p style="text-align:justify;">
                                    {{$athlete->about}}
                                </p>
                                <p style="text-align:justify;">
                                    <strong>Место в рейтинге: </strong>{{$athlete->rating}} (мировой рейтинг)
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
