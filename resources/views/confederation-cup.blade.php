@extends('layouts.app')
@section('main')

    <section class="news competitions">
    <div class="container">

        <div class="white-content">
            <h2>Кубок конфедераций 2018</h2>
            <div class="news__main">
                <div class="item d-flex">
                    <div class="image">
                        <div class="image__item">
                            <img src="/assets/img/competitions/1.png">
                        </div>
                        <div class="image__item">
                            <img src="/assets/img/competitions/2.png">
                        </div>
                        <div class="image__item">
                            <img src="/assets/img/competitions/3.png">
                        </div>
                        <div class="image__item">
                            <img src="/assets/img/competitions/4.png">
                        </div>
                    </div>
                    <div class="desc">
                        <!--<h3><a href="#">КУБОК КОНФЕДЕРАЦИЙ <span>2018</span></a></h3>-->
                        <p class="big">Общий призовой фонд 145 млн тенге!</p>
                        <p>Кубок Конфедерации - уникальный турнир по по боксу, греко-римской, вольной борьбе, казах
                            курес, дзюдо и таеквондо и тяжелой атлетике, которые проходят на всех площадках
                            одновременно. За выход в финальный турнир борются команды областей и городов Астана, Алматы
                            и Шымкент. Отборочные турниры сезона 2018 состоятся в октябре.</p>
                        <p>Победители групп получат путевки на финальный этап, который состоится декабре в городе
                            Алматы.</p>
                        <p class="gold">Поддержи свою команду!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('.competitions .image').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplayspeed: 2000,
        fade: true
    })
</script>

<section>
    <div class="container">
        <div class="white-content">
            <h2>Информация о кубке</h2>
            <div class="banner">
                <img src="/assets/img/banners/1.jpg">
            </div>
            <div class="plain-text">
                <h4>Уважаемые участники и гости!</h4>
                <p>Мы искренне рады приветствовать Вас на втором командном турнире «Кубок Конфедерации спортивных
                    единоборств и силовых видов спорта», проводимом по видам: бокс, дзюдо, вольная борьба, греко-римская
                    борьба, таеквондо, казах курес. Это событие, в очередной раз, дало возможность встретиться лучшим
                    атлетам страны, чтобы в напряженной борьбе вести борьбу за обладание «Кубком Конфедерации-2017»</p>
                <p>Мы надеемся, что все участники турнира, которое предполагает стать величайшим спортивным событием в
                    жизни нашей республики и на котором будут состязаться самые лучшие спортсмены страны, будут
                    вознаграждены хорошими результатами за свои неутомимые ежедневные тренировки.</p>
                <p>Пусть предстоящий турнир «Кубок Конфедерации - 2017» позволит участникам продемонстрировать свое
                    высокое мастерство, силу духа, волю к победе, а нам увидеть красивейшие поединки сильнейших атлетов.
                    Бесспорно, это будет день побед и поражений, праздник силы духа и опыта, драматизма и спортивного
                    азарта.</p>
                <p>Желаем всем участникам спортивных баталий оптимизма и хорошего настроения, крепости духа и
                    неиссякаемой энергии, стремления и воли к победе!</p>
                <p>Пусть удача и вера в себя станут счастливыми спутницами участников турнира!</p>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="white-content">
            <h2>Групповые этапы</h2>
            <img src="/assets/img/confederations-cup/bg1.png" class="bg">
            <p class="gold">Уважаемые участники и гости!</p>
            <p><strong>Группа «А»:</strong> Алматинская обл., ЗКО, Павлодарская обл., Атырауская обл.;</p>
            <p><strong>Группа «B»:</strong> Мангыстауская обл., СКО, Карагандинская обл., ВКО;</p>
            <p><strong>Группа «С»:</strong> ЮКО; Актюбинская обл., г. Астана, Костанайская обл.;</p>
            <p><strong>Группа «D»:</strong> Жамбылская обл. Кызылординская обл., г. Алматы, Акмолинская обл.;</p>
            <p>&nbsp;</p>
            <p><strong>В полуфинал прошли следующие команды:</strong></p>
            <p><strong>Бокс:</strong> Алматинская обл., ЮКО, Мангыстауская обл., г.Астана</p>
            <p><strong>Таеквондо:</strong> г. Алматы, Алматинская обл., ЮКО, Мангыстауская обл.;</p>
            <p><strong>Дзюдо:</strong> г. Алматы, Атырауская обл., Актюбинская обл., Карагандинская обл.</p>
            <p><strong>Греко-римская борьба:</strong> Кызылординская обл., Алматинская обл., ЮКО, Карагандинская обл.;
            </p>
            <p><strong>Вольная борьба:</strong> г. Алматы, Алматинская обл., Актюбинская обл., ВКО;</p>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="white-content">
            <h2>Программа Кубка Конфедерации
                спортивных единоборств и силовых видов спорта</h2>
            <img src="/assets/img/confederations-cup/bg2.png" class="bg">
            <p class="gold">1 декабря</p>
            <p><strong>10:00-10:30</strong> Аккредитация в гостинице «Астана»</p>
            <p><strong>10:30-14:00</strong> Мандатная комиссия в гостинице «Астана»</p>
            <p><strong>16:00-17:00</strong> Общее собрание для судей и тренеров в гостинице «Астана»</p>
            <p><strong>17:30-18:00</strong> Предварительное взвешивание спортсменов вгостинице «Астана»</p>
            <p><strong>18:00-18:30</strong> Официальное взвешивание в гостинице «Астана»</p>
            <p><strong>19:30-20:00</strong> Пресс-конференция в ТРЦ «Мега Алматы 2»</p>
            <p>&nbsp;</p>
            <p class="gold">2 декабря</p>
            <p><strong>10:00-10:30</strong> Аккредитация в гостинице «Астана»</p>
            <p><strong>10:30-14:00</strong> Мандатная комиссия в гостинице «Астана»</p>
            <p><strong>16:00-17:00</strong> Общее собрание для судей и тренеров в гостинице «Астана»</p>
            <p><strong>17:30-18:00</strong> Предварительное взвешивание спортсменов вгостинице «Астана»</p>
            <p><strong>18:00-18:30</strong> Официальное взвешивание в гостинице «Астана»</p>
            <p><strong>19:30-20:00</strong> Пресс-конференция в ТРЦ «Мега Алматы 2»</p>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="white-content">
            <h2>Кубок Конфедераций 2018</h2>
            <img src="/assets/img/confederations-cup/bg3.png" class="bg">
            <div class="d-flex logo-right align-items-center">
                <div>
                    <p class="gold">БОКС</p>
                    <p><strong>1 место - Мангыстауская область</strong></p>
                    <p><strong>2 место - г. Астана</strong></p>
                    <p><strong>3 место - Алматинская обл., ЮКО</strong></p>
                </div>
                <div class="logo"><img src="/assets/img/federations/1.png"></div>
            </div>

            <div class="d-flex logo-right align-items-center">
                <div>
                    <p class="gold">Дзюдо</p>
                    <p><strong>1 место - Жамбылская область</strong></p>
                    <p><strong>2 место - Карагандинская область</strong></p>
                    <p><strong>3 место - Кызылординская обл., ВКО</strong></p>
                </div>
                <div class="logo"><img src="/assets/img/federations/2.png"></div>
            </div>

            <div class="d-flex logo-right align-items-center">
                <div>
                    <p class="gold">ТАЕКВОНДО</p>
                    <p><strong>1 место - Жамбылская область</strong></p>
                    <p><strong>2 место - ЮКО</strong></p>
                    <p><strong>3 мсето - г.Алматы, ЗКО</strong></p>
                </div>
                <div class="logo"><img src="/assets/img/federations/3.png"></div>
            </div>

        </div>
    </div>
</section>

<section class="photos">
    <div class="container">
        <div class="white-content">
            <div class="title-block d-flex justify-content-between">
                <h2>Федерации</h2>
                <div class="title-block__carousel-controls d-flex">
                    <span class="fas fa-angle-left" onclick="$('.photos__carousel').trigger('prev.owl.carousel', [500]);"></span>
                    <span class="fas fa-angle-right" onclick="$('.photos__carousel').trigger('next.owl.carousel', [500]);"></span>
                </div>
            </div>
            <div class="photos__carousel owl-carousel">
                <a href="/assets/img/confederations-cup/gallery/1.jpg" data-fancybox="photos"><img src="/assets/img/confederations-cup/gallery/1.jpg"></a>
                <a href="/assets/img/confederations-cup/gallery/2.jpg" data-fancybox="photos"><img src="/assets/img/confederations-cup/gallery/2.jpg"></a>
                <a href="/assets/img/confederations-cup/gallery/3.jpg" data-fancybox="photos"><img src="/assets/img/confederations-cup/gallery/3.jpg"></a>
                <a href="/assets/img/confederations-cup/gallery/1.jpg" data-fancybox="photos"><img src="/assets/img/confederations-cup/gallery/1.jpg"></a>
                <a href="/assets/img/confederations-cup/gallery/2.jpg" data-fancybox="photos"><img src="/assets/img/confederations-cup/gallery/2.jpg"></a>
                <a href="/assets/img/confederations-cup/gallery/3.jpg" data-fancybox="photos"><img src="/assets/img/confederations-cup/gallery/3.jpg"></a>
            </div>
        </div>
    </div>
</section>

<script>
    var owl = $('.photos__carousel');
    owl.owlCarousel({
//        dots: true,
        autoWidth:true,
        nav: false,
        margin: 30,
        smartSpeed: 800,
        responsive: {
            1600: {
                items: 4
            },
            1024: {
                items: 4
            },
            768: {
                items: 3,
                margin: 15,
                autoWidth: false
            },
            600: {
                items: 2,
                margin: 10,
                autoWidth: false
            },
            0: {
                items: 1,
                autoWidth: false
            }
        }
    });
    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY > 0) {
            $(this).trigger('next.owl');
        } else {
            $(this).trigger('prev.owl');
        }
        e.preventDefault();
    });
</script>

@endsection

