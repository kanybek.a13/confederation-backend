@extends('layouts.app')
@section('main')

<section class="media-blocks">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Видео</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Видео</span></li>
                </ul>
            </div>
            <div class="row">
                @forelse($videos as $video)
                    <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                        <a href="{{$video->source}}" data-fancybox="video">
                            <div class="img">
                                <img src="{{$video->source}}">
                                <i class="fas fa-video"></i>
                            </div>
                                <h3 class="title">{{$video->name}}</h3>
                        </a>
                        <div class="post-details">
                            <span class="date">{{$video->created_at}}</span>
                            <span><i class="far fa-eye"></i> {{$video->views}}</span>
                            <span><i class="fas fa-comments"></i> {{$video->comment()->count()}}</span>
                        </div>
                    </div>
                @empty
                    <p>No videos yet!</p>
                @endforelse
            </div>
        </div>
    </div>
</section>

@endsection
