@extends('layouts.app')
@section('main')

<section class="media-blocks">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Фото</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Фото</span></li>
                </ul>
            </div>

            <div class="row">

                @forelse($photos as $photo)
                    <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                        <a href="media/{{$photo->id}}">
                            <div class="img">
                                <img src="{{$photo->source}}">
                                <i class="far fa-image"></i>
                            </div>
                            <h3 class="title">{{$photo->name}}</h3>
                        </a>
                        <div class="post-details">
                            <span class="date">{{$photo->created_at}}</span>
                            <span><i class="far fa-eye"></i> {{$photo->views}}</span>
                            <span><i class="fas fa-comments"></i> {{$photo->comment()->count()}}</span>
                        </div>
                    </div>
                @empty
                    <p>No photos yet!</p>
                @endforelse
            </div>
        </div>
    </div>
</section>

@endsection
