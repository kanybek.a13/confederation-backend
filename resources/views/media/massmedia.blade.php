@extends('layouts.app')
@section('main')

<section class="media-blocks">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Фото</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{currentFederation->site}}">Главная</a></li>
                    <li><span>Фото</span></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                    <a href="#">
                        <div class="img">
                            <img src="/assets/img/massmedia/1.jpg">
                        </div>
                        <h3 class="title">УТС мужской сборной РК</h3>
                    </a>
                    <div class="post-details">
                        <span class="date">3 сентября</span>
                        <span><i class="far fa-eye"></i> 1290</span>
                        <span><i class="fas fa-comments"></i> 12</span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                    <a href="#">
                        <div class="img">
                            <img src="/assets/img/massmedia/2.jpg">
                        </div>
                        <h3 class="title">Открытие спортивной школы-интернат в Петропавловске</h3>
                    </a>
                    <div class="post-details">
                        <span class="date">10 октября</span>
                        <span><i class="far fa-eye"></i> 2131</span>
                        <span><i class="fas fa-comments"></i> 6</span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                    <a href="#">
                        <div class="img">
                            <img src="/assets/img/massmedia/1.jpg">
                        </div>
                        <h3 class="title">УТС мужской сборной РК</h3>
                    </a>
                    <div class="post-details">
                        <span class="date">3 сентября</span>
                        <span><i class="far fa-eye"></i> 1290</span>
                        <span><i class="fas fa-comments"></i> 12</span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                    <a href="#">
                        <div class="img">
                            <img src="/assets/img/massmedia/2.jpg">
                        </div>
                        <h3 class="title">Нурсултан Турсынов победитель Чемпионата Азии по видам борьбы</h3>
                    </a>
                    <div class="post-details">
                        <span class="date">3 сентября</span>
                        <span><i class="far fa-eye"></i> 1290</span>
                        <span><i class="fas fa-comments"></i> 12</span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                    <a href="#">
                        <div class="img">
                            <img src="/assets/img/massmedia/1.jpg">
                        </div>
                        <h3 class="title">УТС мужской сборной РК</h3>
                    </a>
                    <div class="post-details">
                        <span class="date">3 сентября</span>
                        <span><i class="far fa-eye"></i> 1290</span>
                        <span><i class="fas fa-comments"></i> 12</span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                    <a href="#">
                        <div class="img">
                            <img src="/assets/img/massmedia/2.jpg">
                        </div>
                        <h3 class="title">Открытие спортивной школы-интернат в Петропавловске</h3>
                    </a>
                    <div class="post-details">
                        <span class="date">10 октября</span>
                        <span><i class="far fa-eye"></i> 2131</span>
                        <span><i class="fas fa-comments"></i> 6</span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                    <a href="#">
                        <div class="img">
                            <img src="/assets/img/massmedia/1.jpg">
                        </div>
                        <h3 class="title">УТС мужской сборной РК</h3>
                    </a>
                    <div class="post-details">
                        <span class="date">3 сентября</span>
                        <span><i class="far fa-eye"></i> 1290</span>
                        <span><i class="fas fa-comments"></i> 12</span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                    <a href="#">
                        <div class="img">
                            <img src="/assets/img/massmedia/2.jpg">
                        </div>
                        <h3 class="title">Нурсултан Турсынов победитель Чемпионата Азии по видам борьбы</h3>
                    </a>
                    <div class="post-details">
                        <span class="date">3 сентября</span>
                        <span><i class="far fa-eye"></i> 1290</span>
                        <span><i class="fas fa-comments"></i> 12</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
