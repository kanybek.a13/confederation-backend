@extends('layouts.app')
@section('main')

<section class="media-blocks">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Медиа</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Медиа</span></li>
                </ul>
            </div>

            <div class="row">

                @forelse($medias as $media)
                    @if($media->type==='photo')
                        <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                            <a href="/{{$currentFederation->site}}/media/{{$media->id}}">
                                <div class="img">
                                    <img src="{{$media->image}}">
                                    <i class="far fa-image"></i>
                                </div>
                                <h3 class="title">{{$media->name}}</h3>
                            </a>
                            <div class="post-details">
                                <span class="date">{{$media->created_at}}</span>
                                <span><i class="far fa-eye"></i> {{$media->views}} </span>
                                <span><i class="fas fa-comments"></i> {{$media->comment()->count()}} </span>
                            </div>
                        </div>
                    @elseif($media->type==='video')
                        <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                            <a href="{{$media->video->source}}" data-fancybox="video">
                                <div class="img">
                                    <img src="{{$media->image}}">
                                    <i class="fas fa-video"></i>
                                </div>
                                <h3 class="title">{{$media->name}}</h3>
                            </a>
                            <div class="post-details">
                                <span class="date">3 сентября</span>
                                <span><i class="far fa-eye"></i> {{$media->views }} </span>
                                <span><i class="fas fa-comments"></i> {{$media->comment()->count()}} </span>
                            </div>
                        </div>
                    @endif
                @empty
                    <p>No media yet!</p>
                @endforelse
            </div>
        </div>
    </div>
</section>

@endsection
