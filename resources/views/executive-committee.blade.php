@extends('layouts.app')
@section('main')

<section class="executive-committee">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Исполнительный комитет</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Исполнительный комитет</span></li>
                </ul>
            </div>

            @forelse($executiveCommittee as $user)
                <div class="card-regular">
                    <img class="img" src="{{$user->image}}">
                    <div>
                        <h3 class="title">{{$user->fullname}}</h3>
                        <p class="grey"><strong>{{$user->position}}</strong></p>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>

@endsection
