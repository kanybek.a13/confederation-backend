@extends('layouts.app')
@section('main')

    <section class="apparatus">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Аппарат конфедерации</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Аппарат конфедерации</span></li>
                </ul>
            </div>
            <div class="card-regular">
                <h3 class="card-regular__title">Руководство</h3>
                <div class="row">
                    @forelse($apparatuses as $apparatus)
{{--                    @forelse($lead_apparatuses as $lead_apparatus)--}}
                        @if($apparatus['position_notes']='Руководство')
                            <div class="col-lg-6 d-flex">
                                <img class="img" src="{{$lead_apparatus->image}}">
                                <div class="align-self-center">
                                    <h4 class="title">{{$lead_apparatus->fullname}}</h4>
                                    <p class="grey"><strong>{{$lead_apparatus->position}}</strong></p>
                                    <p class="grey">
                                        тел.: <a href="tel:+7 (7172) 280-994">{{$lead_apparatus->phone}}</a>
                                    </p>
                                </div>
                            </div>
                        @endif
                    @empty
                        <p>No apparatus yet!</p>
                    @endforelse
                </div>
            </div>
            <div class="card-regular">
                <h3 class="card-regular__title">Служба финансового обеспечения</h3>
                <div class="row">
                    @forelse($apparatuses as $financial_support_apparatus)
                        @if($apparatus['position_notes']='Служба финансового обеспечения')
                            <div class="col-lg-6 d-flex">
                                <img class="img" src="{{$financial_support_apparatus->image}}">
                                <div class="align-self-center">
                                    <h4 class="title">{{$financial_support_apparatus->fullname}}</h4>
                                    <p class="grey">
                                        тел.: <a href="tel:+7 (7172) 280-993">{{$financial_support_apparatus->phone}}</a><br/>
                                        e-mail: <a href="mailto:finance@confederation.kz">{{$financial_support_apparatus->email}}</a>
                                    </p>
                                </div>
                            </div>
                        @endif
                    @empty
                        <p>No apparatus yet!</p>
                    @endforelse
                </div>
            </div>
            <div class="card-regular">
                <h3 class="card-regular__title">Служба правового обеспечения</h3>
                <div class="row">
                    @forelse($apparatuses as $legal_support_apparatus)
                        @if($apparatus['position_notes']='Служба правового обеспечения')
                            <div class="col-lg-6 d-flex">
                                <img class="img" src="{{$legal_support_apparatus->image}}">
                                <div class="align-self-center">
                                    <h4 class="title">{{$legal_support_apparatus->fullname}}</h4>
                                    <p class="grey">
                                        тел.: <a href="tel:+7 (7172) 280-993">{{$legal_support_apparatus->phone}}</a><br/>
                                        e-mail: <a href="mailto:finance@confederation.kz">{{$legal_support_apparatus->email}}</a>
                                    </p>
                                </div>
                            </div>
                        @endif
                    @empty
                        <p>No apparatus yet!</p>
                    @endforelse
                </div>
            </div>
            <div class="card-regular">
                <h3 class="card-regular__title">Служба по работе со спортивными организациями</h3>
                <div class="row">
                    @forelse($apparatuses as $sport_services_apparatus)
                        @if($apparatus['position_notes']='Служба по работе со спортивными организациями')
                            <div class="col-lg-6 d-flex">
                                <img class="img" src="{{$sport_services_apparatus->image}}">
                                <div class="align-self-center">
                                    <h4 class="title">{{$sport_services_apparatus->fullname}}</h4>
                                    <p class="grey">
                                        тел.: <a href="tel:+7 (7172) 280-993">{{$sport_services_apparatus->phone}}</a><br/>
                                        e-mail: <a href="mailto:finance@confederation.kz">{{$sport_services_apparatus->email}}</a>
                                    </p>
                                </div>
                            </div>
                        @endif
                    @empty
                        <p>No apparatus yet!</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
