@extends('layouts.app')
@section('main')

<div class="container">
</div>

<section>
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Спортивные секции</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Спортивные секции</span></li>
                </ul>
            </div>

            <form class="row margin">
                <div class="col-md-4 col-lg-3 text-uppercase">
                    <div class="input-group">
                        <select name="city_id" data-placeholder="Все города" class="chosen">
                            <option value="0" selected>Все города</option>
                            @forelse($cities as $city)
                                <option value="{{$city->id}}" {{$city->id==request('city_id') ? 'selected' : ''}}>{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4 col-lg-2">
                    <div class="input-group">
                        <input class="" type="text" placeholder="Название" name="name" value="{{request('name')}}">
                    </div>
                </div>
                <div class="col-md-4 col-lg-2">
                    <div class="input-group">
                        <input class="" type="text" placeholder="Адрес" name="address" value="{{request('address')}}">
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 text-uppercase">
                    <div class="input-group">
                        <select name="federation_id" data-placeholder="Все виды спорта" class="chosen">
                            <option value="all" selected>Все виды спорта</option>
                            @forelse($allFederations as $federation)
                                <option value="{{$federation->id}}" {{request('federation_id')==$federation->id ? 'selected' : ''}}>{{$federation->sport}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-lg-2">
                    <div class="input-group">
                        <button type="submit" href="#" class="button-primary">Найти
                        </button>
                    </div>
                </div>
            </form>

{{--            <div class="margin map">--}}
{{--                <script type="text/javascript" charset="utf-8" async--}}
{{--                        src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A960ecd0892c92a2b90e6a83d455ecb5332cc5871fbcde81766888f356283f227&amp;width=100%25&amp;height=537&amp;lang=ru_RU&amp;scroll=true"></script>--}}
{{--            </div>--}}

            <div class="tab-content" id="myTabContent">
                <div class="container">
                    <div class="row">
                         @forelse($sections as $section)
                            <div class="card-regular col-lg-6 white-content" style="margin-bottom:2px;" >
                                <a href="/{{$currentFederation->site}}/section/{{$section->id}}" class="d-flex align-items-center">
                                    <img src="{{$section->image}}" width="150" height="150">
                                    <div style="margin-left: 5%">
                                        <h3>{{$section->name}}</h3>
                                        <div class="grey">
                                            <p>{{$section->address}}</p>
                                            <p>{{$section->phone}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @empty
                            <p>No sections yet!</p>
                        @endforelse
                    </div>
                </div>
            </div>
            </div>

            <ul class="pagination">
                <li class="previous_page disabled"><a href="#"><i class="fas fa-long-arrow-alt-left"></i></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">6</a></li>
                <li><a href="#">7</a></li>
                <li><a href="#">8</a></li>
                <li class="dots disabled"><a href="#">...</a></li>
                <li><a href="#">498</a></li>
                <li><a href="#">499</a></li>
                <li class="next_page"><a href="#"><i class="fas fa-long-arrow-alt-right"></i></a></a></li>
            </ul>
        </div>
    </div>
</section>

@endsection
