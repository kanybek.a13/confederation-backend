@extends('layouts.app')
@section('main')

<section>
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>История Федерации</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>История Федерации</span></li>
                </ul>
            </div>

            @if($history)
                <div class="banner">
                    <img src="{{$history->image}}">
                </div>
                <div class="plain-text">
                    <h4>{{$history->title}}</h4>
                    <p>{{$history->body}}</p>
                </div>
            @else
                No history yet!
            @endif
        </div>
    </div>
</section>

@endsection
