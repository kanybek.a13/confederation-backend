@include('admin.layouts.header')

<main class="main">
    <div class="container ">
        <section>
            <h2 class="title-primary">Тренеры</h2>
            <form action="{{ route('admin.coaches', [$currentFederation->site]) }}" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">ФИО</label>
                            <input type="text" name="fullname" class="input-regular" placeholder="ФИО"  value="{{request('fullname')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Список тренеров</h2>
            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                    <tr>
                        <th>№</th>
                        <th>ФИО</th>
                        <th>Должность</th>
                        <th>Федерация</th>
                        <th>Команда</th>
                        <th>Действия</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($allCoaches as $coach)
                        <tr>
                            <td>{{$coach->id}}</td>
                            <td>{{$coach->fullname}}</td>
                            <td>{{$coach->position}}</td>
                            <td>{{$coach->federation->name}}</td>
                            <td>
                                @forelse($coach->team as $team)
                                    <a href="{{ route('admin.team.show', [$currentFederation->site, $team->id]) }}" title="{{$team->name}}">{{$team->name}}</a>
                                    <br>
                                @empty
                                    No Teams
                                @endforelse
                            </td>
                            <td>
                                <div class="action-buttons">
                                    <a href="{{ route('admin.coach.show', [$currentFederation->site, $coach->id]) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="{{ route('admin.coach.edit', [$currentFederation->site, $coach->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <form action="{{ route('admin.coach.delete', [$currentFederation->site, $coach->id]) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <input type="hidden" name="id" value="{{$coach->id}}">
                                        <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$allCoaches->currentPage() === 1 ? 'disabled' : '' }} href="{{ route('admin.coaches', [$currentFederation->site]) }}?page={{ $allCoaches->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $allCoaches->lastPage(); $page++)
                    <li><a {{ $page === $allCoaches->currentPage() ? 'class="active"' : '' }} href="{{ route('admin.coaches', [$currentFederation->site]) }}?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$allCoaches->currentPage() === $allCoaches->lastPage() ? 'disabled' : '' }} href="{{ route('admin.coaches', [$currentFederation->site]) }}?page={{ $allCoaches->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
