@include('admin.layouts.header')

@if ($errors->any())
    <ul class="field mt-6 list-reset">
        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
            <li class="sm:text-xs text-red">{{ $error }}</li>
        @endforeach
    </ul>
@endif


<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.medias', [$currentFederation->site]) }}" title="Медиа"> Медиа</a></li>
        <li><span>Новый Видео</span></li>
    </ul>

    <form class="block" method="post" action="{{ route('admin.media.store', [$currentFederation->site]) }}" enctype="multipart/form-data">
        @csrf
        @method('post')

        <div class="tabs-contents">
            <div class="active">
                <input type="hidden" name="type" value="video">
                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="name" value="" placeholder="Название" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Ссылка на видео</label>
                    <input type="url" name="source" value="" placeholder=" Ссылка на видео" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="file-input"> <span class="file-selected">Прикрепить Фото</span>
                        <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                        <input type="file" style="display:none;" class="fileUpload" name="image" required>
                    </label>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>

        @if ($errors->{ $bag ?? 'default' }->any())
            <ul class="field mt-6 list-reset">
                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                    <li class="sm:text-xs text-red">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </form>
</div>
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
