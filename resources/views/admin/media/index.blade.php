@include('admin.layouts.header')

<main class="main">
    <div class="container ">
        <section>
            <h2 class="title-primary">Медиа</h2>
            <form action="{{ route('admin.medias', [$currentFederation->site]) }}" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Название</label>
                            <input type="text" name="name" class="input-regular" placeholder="Название"  value="{{request('name')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Список Медиа</h2>
            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Тип</th>
                    <th>Федерация</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                @forelse($allMedia as $media)
                    <tr>
                        <td>{{$media->id}}</td>
                        <td>{{$media->name}}</td>
                        <td>{{$media->type === 'video' ? 'Видео' : 'Фото'}}</td>
                        <td>{{$media->federation->name}}</td>
                        <td>
                            <div class="action-buttons">
                                <a href="{{ route('admin.media.show', [$currentFederation->site, $media->id]) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                <a href="{{ route('admin.media.edit', [$currentFederation->site, $media->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                <form action="{{ route('admin.media.delete', [$currentFederation->site, $media->id]) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id" value="{{$media->id}}">
                                    <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    No media yet!
                @endforelse
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$allMedia->currentPage() === 1 ? 'disabled' : '' }} href="{{ route('admin.medias', [$currentFederation->site]) }}?page={{ $allMedia->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $allMedia->lastPage(); $page++)
                    <li><a {{ $page === $allMedia->currentPage() ? 'class="active"' : '' }} href="{{ route('admin.medias', [$currentFederation->site]) }}?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$allMedia->currentPage() === $allMedia->lastPage() ? 'disabled' : '' }} href="{{ route('admin.medias', [$currentFederation->site]) }}?page={{ $allMedia->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
        <!---->
@endsection
