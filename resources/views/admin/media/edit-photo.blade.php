@include('admin.layouts.header')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.medias', [$currentFederation->site]) }}" title="Медиа"> Медиа</a></li>
        <li><span>Изменить Фото</span></li>
    </ul>

    <form class="block" method="post" action="{{ route('admin.media.update', [$currentFederation->site, $media->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="tabs-contents">
            <div class="active">
                <input type="hidden" name="type" value="photo">
                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="name" value="{{$media->name}}" placeholder="Название" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="file-input"> <span class="file-selected">Прикрепить Фото</span>
                        <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                        <input type="file" style="display:none;" class="fileUpload" name="images[]" multiple required>
                    </label>
                </div>

                <hr>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>

        @if ($errors->{ $bag ?? 'default' }->any())
            <ul class="field mt-6 list-reset">
                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                    <li class="sm:text-xs text-red">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </form>
</div>

<div class="container container-fluid" >
    <br>
    <div class="active">
        <div class="input-group">
            <label class="input-group__title"> Фотографии</label>

            @forelse($media->photos as $photo)
                <div class="col-sm-6 col-md-4 col-lg-3 media-blocks-item">
                    <div class="img">
                        <img src="{{$photo->path}}" height="200" width="200">
                        <i class="far fa-image"></i>
                    </div>
                    <div class="action-buttons">
                        <form action="{{ route('admin.media.photo.delete', [$currentFederation->site, $media->id, $photo->id]) }}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                        </form>
                    </div>
                </div>
            @empty
                No photos yet!
            @endforelse
        </div>
    <hr>
</div>

    @if ($errors->{ $bag ?? 'default' }->any())
        <ul class="field mt-6 list-reset">
            @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                <li class="sm:text-xs text-red">{{ $error }}</li>
            @endforeach
        </ul>
    @endif
</div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
