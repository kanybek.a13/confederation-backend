@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.teams', [$currentFederation->site]) }}" title="Команды">Команды</a></li>
        <li><span>{{$team->name}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$team->id}}</div>
            <h1 class="fund-header__title">{{$team->name}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{$team->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения </div>
                <div class="property__text">{{$team->updated_at}}<br></div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                    </ul>
                    <div class="input-group">
                        <div class="row row--multiline">
                            <div class="col-md-4 col-sm-6">
                                <a href="{{ route('admin.team.edit', [$currentFederation->site, $team->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit" align="right" ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title"> Название</label>
                        <input type="text" name="title" value="{{$team->name}}" placeholder="Название" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Федерация</label>
                        <input type="text" name="federation_id" value="{{$team->federation->name}}" placeholder="Федерация" class="input-regular" disabled>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>

    <div class="block">
        <form class="block" method="post" action="{{ route('admin.team.athlete.invite', [$currentFederation->site, $team->id]) }}">
            @csrf
            @method('post')

            <div class="input-group">
                <label class="input-group__title">Добавить в команды</label>
                <br>
                <select name="athlete_id" class="chosen no-search input-regular" data-placeholder="" required>
                    <option value="" selected disabled>Выберите атлета</option>

                    @forelse($federationAthletes as $athlete)
                        <option value="{{$athlete->id}}">{{$athlete->fullname }}</option>
                    @endforeach
                </select>
            </div>
            <div class="buttons">
                <div>
                    <button type="submit" class="btn btn--green">Добавить</button>
                </div>
            </div>
        </form>
    </div>

    <div class="block">
        <h2 class="title-secondary">Состав команды</h2>
        <table class="table records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 15%;">
                <col span="1" style="width: 12%;">
                <col span="1" style="width: 12%;">
                <col span="1" style="width: 12%;">
                <col span="1" style="width: 12%;">
                <col span="1" style="width: 15%;">
            </colgroup>
            <thead>
            <tr>
                <th>№</th>
                <th>ФИО</th>
                <th>Дата рождение</th>
                <th>Весовая категория</th>
                <th>Рейтинг</th>
                <th>Месторождение</th>
                <th>Федерация</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($athletes as $athlete)
                <tr>
                    <td>{{$athlete->id}}</td>
                    <td>
                        <a href="{{ route('admin.athlete.show', [$currentFederation->site, $athlete->id]) }}">
                            {{$athlete->fullname}}
                        </a>
                    </td>
                    <td>{{$athlete->date_of_birth}}</td>
                    <td>{{$athlete->weight}}</td>
                    <td>{{$athlete->rating}}</td>
                    <td>{{$athlete->birthplace}}</td>
                    <td>{{$athlete->federation->name}}</td>
                    <td>
                        <div class="action-buttons">
                            <form action="{{ route('admin.team.athlete.delete', [$currentFederation->site, $team->id , $athlete->id ] )}}" method="post">
                                @csrf
                                @method('post')
                                <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-close"  title="Удалить из команды" ></button>
                            </form>
                        </div>
                    </td>
                </tr>
            @empty
                <div class="text-align:center">No athletes yet.</div>
            @endforelse
            </tbody>
        </table>
    </div>
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
