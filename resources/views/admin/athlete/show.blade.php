@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.athletes', [$currentFederation->site]) }}" title="Атлеты">Атлеты</a></li>
        <li><span>{{$athlete->fullname}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$athlete->id}}</div>
            <h1 class="fund-header__title">{{$athlete->fullname}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{$athlete->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения </div>
                <div class="property__text">{{$athlete->updated_at}}<br></div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                    </ul>
                    <div class="input-group">
                        <div class="row row--multiline">
                            <div class="col-md-4 col-sm-6">
                                <a href="{{ route('admin.athlete.edit', [$currentFederation->site, $athlete->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit" align="right" ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="col-sm-4">
                        <img class="article-image" src="{{$athlete->image}}" width="200" height="200">
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> ФИО</label>
                        <input type="text" name="fullname" value="{{$athlete->fullname}}" placeholder="ФИО" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Дата рождение</label>
                        <input type="text" name="date_of_birth" value="{{$athlete->date_of_birth}}" placeholder="Дата рождение" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Месторождение</label>
                        <input type="text" name="birthplace" value="{{$athlete->birthplace}}" placeholder="Месторождение" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Вес</label>
                        <input type="text" name="weight" value="{{$athlete->weight}}" placeholder="Вес" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Рейтинг</label>
                        <input type="text" name="rating" value="{{$athlete->rating}}" placeholder="Рейтинг" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Федерация</label>
                        <input type="text" name="federation_id" value="{{$athlete->federation->name}}" placeholder="Федерация" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> About</label>
                        <textarea name="about" placeholder="About" class="input-regular" disabled>{{$athlete->about}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
        <!---->
@endsection
