@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.athletes', [$currentFederation->site]) }}" title="Атлеты">Атлеты</a></li>
        <li><a href="{{ route('admin.athlete.show', [$currentFederation->site, $athlete->id]) }}" title="{{$athlete->fullname}}">{{$athlete->fullname}}</a></li>
        <li><span>Edit</span></li>
    </ul>
    <div class="tabs-contents">
        <div class="active">
            <form class="block" method="post" action="{{ route('admin.athlete.update', [$currentFederation->site, $athlete->id]) }}">
                @csrf
                @method('put')

                <div class="input-group">
                    <label class="input-group__title"> ФИО</label>
                    <input type="text" name="fullname" value="{{$athlete->fullname}}" placeholder="ФИО" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Дата рождение</label>
                    <input type="date" name="date_of_birth" value="{{$athlete->date_of_birth}}" placeholder="Дата рождение" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Месторождение</label>
                    <input type="text" name="birthplace" value="{{$athlete->birthplace}}" placeholder="Месторождение" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Вес</label>
                    <input type="number" name="weight" value="{{$athlete->weight}}" placeholder="Вес" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Рейтинг</label>
                    <input type="number" name="rating" value="{{$athlete->rating}}" placeholder="Рейтинг" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Федерация</label>
                    <select name="federation_id" class="chosen no-search input-regular" data-placeholder="Все" required>
                        @forelse($allFederations as $federation)
                            <option value="{{$federation->id}}" {{$currentFederation->id ===  $federation->id ? 'selected' :''}} >{{$federation->name }}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> About</label>
                    <textarea name="about" placeholder="About" class="input-regular">{{$athlete->about}}</textarea>
                </div>
                <br>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>

                @if ($errors->{ $bag ?? 'default' }->any())
                    <ul class="field mt-6 list-reset">
                        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                            <li class="sm:text-xs text-red">{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </form>
        </div>
    </div>
</div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
