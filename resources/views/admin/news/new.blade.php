@include('admin.layouts.header')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><a href="{{ route('admin.news', $currentFederation->site) }}" title="Новости"> Новости</a></li>
        <li><span>Новый</span></li>
    </ul>

    <form class="block" method="post" action="{{ route('admin.news', $currentFederation->site) }}" enctype="multipart/form-data">
        @csrf
        @method('post')

        <div class="tabs-contents">
            <div class="active">
                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="title" value="{{ old('title') }}" placeholder="Название" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Текст новости</label>
                    <textarea type="text" name="body" class="input-regular" placeholder="Текст новости" required>{{ old('title') }}</textarea>
                </div>
                <br>
                <div class="input-group">
                    <label class="file-input"> <span class="file-selected">Прикрепить Фото</span>
                        <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                        <input type="file" style="display:none;" class="fileUpload" name="image[]" multiple>
                    </label>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>

        @if ($errors->{ $bag ?? 'default' }->any())
            <ul class="field mt-6 list-reset">
                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                    <li class="sm:text-xs text-red">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </form>
</div>
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
