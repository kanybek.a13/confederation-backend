@include('admin.layouts.header')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><a href="{{ route('admin.news', [$currentFederation->site]) }}" title="Новости">Новости</a></li>
        <li><span>{{$news->title}}</span></li>
    </ul>
    <div class="tabs-contents">
        <div class="active">
            <form class="block" method="post" action="{{route('admin.news.update', [$currentFederation->site, $news->id])}}">
                @csrf
                @method('put')

                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="title" value="{{$news->title}}" placeholder="Название" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Текст новости</label>
                    <textarea type="text" name="body" class="input-regular" placeholder="Текст новости" required>{{$news->body}}</textarea>
                </div>
                <br>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>

                @if ($errors->{ $bag ?? 'default' }->any())
                    <ul class="field mt-6 list-reset">
                        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                            <li class="sm:text-xs text-red">{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </form>
        </div>
    </div>
</div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
