@include('admin.layouts.header');


<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">

@include('admin.layouts.sidebar');

<div class="right-wrapper">
    <header class="header">
        <div class="container container-fluid">
            <a href="javascript:;" title="Свернуть/развернуть навигацию" class="menu-btn icon-menu"></a>
            <a href="/" title="Главная" class="logo hidden-md hidden-lg"><img src="/admin/img/logo-blue.svg" alt=""></a>
            <div class="language hidden-sm hidden-xs">
                <a href="#" title="РУС" class="active">РУС</a>
                <a href="#" title="ENG">ENG</a>
            </div>
            <div class="header-dropdown account-nav">
                <div class="header-dropdown__title">
                    <span>Добро пожаловать, Илья!</span> <img src="/admin/img/user.svg" alt=""> <i
                        class="icon-chevron-down"></i>
                </div>
                <div class="header-dropdown__desc">
                    <ul>
                        <li class="language hidden-md hidden-lg"><a href="#" title="РУС" class="active">РУС</a><a href="#" title="ENG">ENG</a></li>
                        <li><a href="#" title="Выйти">Выйти</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <main class="main">


<div class="container container-fluid">
    <form class="block">
        <div class="input-group">
            <label class="input-group__title">Поисковая фраза</label>
            <input type="text" name="text" placeholder="" class="input-regular">
        </div>
        <div class="collapse-block collapsed" style="display: none;" id="collapse1">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                        <label class="input-group__title">Организация</label>
                        <select name="organization" class="input-regular chosen" data-placeholder=" ">
                            <option value="" selected hidden> </option>
                            <option value="value1">value 1</option>
                            <option value="value2">value 2</option>
                            <option value="value3">value 3</option>
                            <option value="value4">value 4</option>
                            <option value="value5">value 5</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">Год создания: с</label>
                                <label class="date">
                                    <input type="text" name="creationDateFrom" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">до</label>
                                <label class="date">
                                    <input type="text" name="creationDateTo" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">Дата сдачи в архив: с</label>
                                <label class="date">
                                    <input type="text" name="archiveDateFrom" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">до</label>
                                <label class="date">
                                    <input type="text" name="archiveDateTo" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">Дата уничтожения: с</label>
                                <label class="date">
                                    <input type="text" name="destroyDateFrom" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">до</label>
                                <label class="date">
                                    <input type="text" name="destroyDateTo" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">Дата выдачи: с</label>
                                <label class="date">
                                    <input type="text" name="releaseDateFrom" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">до</label>
                                <label class="date">
                                    <input type="text" name="releaseDateTo" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <label class="input-group__title">&nbsp;</label>
                        <div class="input-group">
                            <label class="checkbox">
                                <input type="checkbox" name="archived">
                                <span>В архиве</span>
                            </label>
                        </div>
                        <div class="input-group">
                            <label class="checkbox">
                                <input type="checkbox" name="released">
                                <span>Выдано</span>
                            </label>
                        </div>
                        <div class="input-group">
                            <label class="checkbox">
                                <input type="checkbox" name="storage">
                                <span>Оперативное хранение</span>
                            </label>
                        </div>
                        <div class="input-group">
                            <label class="checkbox">
                                <input type="checkbox" name="destroyed">
                                <span>Уничтожено</span>
                            </label>
                        </div>
                        <div class="input-group">
                            <label class="checkbox">
                                <input type="checkbox" name="formed">
                                <span>Дело сформировано</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-group">
            <a href="javascript:;" title="Расширенный фильтр" class="grey-link small collapse-btn"
               data-target="collapse1">Расширенный фильтр</a></div>
        <div class="buttons">
            <div><button class="btn btn--green">Искать</button></div>
            <div><button class="btn btn--yellow">Сбросить</button></div>
        </div>
    </form>

    <div class="block">
    <h2 class="title-secondary">Список записей</h2>
    <table class="table records">
        <colgroup>
            <col span="1" style="width: 3%;">
            <col span="1" style="width: 20%;">
            <col span="1" style="width: 12%;">
            <col span="1" style="width: 20%;">
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 15%;">
        </colgroup>
        <thead>
        <tr>
            <th>#</th>
            <th>Наименование фонда</th>
            <th>Номер фонда</th>
            <th>Организация</th>
            <th>Дата создания</th>
            <th>Дата изменения</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>67</td>
            <td>Langosh PLC	</td>
            <td>958000</td>
            <td>Государственное учреждение "Управление цифровизации, оказания государственных услуг и архивов Туркестанской области"</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19 Администратор Panama DC</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <div class="text-right">
        <ul class="pagination">
            <li class="previous_page disabled"><span><i class="icon-chevron-left"></i></span></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li class="dots disabled"><span>...</span></li>
            <li><a href="#">498</a></li>
            <li><a href="#">499</a></li>
            <li class="next_page"><a href="#"><i class="icon-chevron-right"></i></a></li>
        </ul>
    </div>
</div>
</div>


@extends('admin.layouts.footer')

    @section('content')
        <!--Only this page's scripts-->

        <!---->
    @endsection
