@include('admin.layouts.header')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><a href="/admin-panel/federations" title="Федерации"> Федерации</a></li>
        <li><span>Новый</span></li>
    </ul>

    <form class="block" method="post" action="/admin-panel/federation" enctype="multipart/form-data">
        @csrf
        @method('post')

        <div class="tabs-contents">
            <div class="active">
                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="name" value="{{ old('name')}}" placeholder="Название" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Название спорта</label>
                    <input type="text" name="sport" value="{{ old('sport')}}" placeholder="Название спорта" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Номер телефона</label>
                    <input type="text" name="phone" value="{{ old('phone')}}" placeholder="Номер телефона" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Почта</label>
                    <input type="email" name="email" value="{{ old('email')}}" placeholder="Почта" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Факс</label>
                    <input type="text" name="fax" value="{{ old('fax')}}" placeholder="Факс" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Веб-сайт</label>
                    <input type="text" name="website" value="{{ old('site')}}" placeholder="Веб-сайт" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Ключевое слово</label>
                    <input type="text" name="site" value="{{ old('site')}}" placeholder="Ключевое слово" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Адрес</label>
                    <input type="text" name="address" value="{{ old('address')}}" placeholder="Адрес" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="file-input"> <span class="file-selected">Прикрепить логотип</span>
                        <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                        <input type="file" style="display:none;" class="fileUpload" name="logo" required>
                    </label>
                </div>
                <br>
                <div class="input-group">
                    <label class="file-input"> <span class="file-selected">Прикрепить фото</span>
                        <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                        <input type="file" style="display:none;" class="fileUpload" name="image" required>
                    </label>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>

        @if ($errors->{ $bag ?? 'default' }->any())
            <ul class="field mt-6 list-reset">
                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                    <li class="sm:text-xs text-red">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </form>
</div>
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
