@include('admin.layouts.header')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><a href="/admin-panel/federations" title="Доктор">Федерации</a></li>
        <li><span>{{$federation->name}}</span></li>
    </ul>

    <div class="tabs-contents">
        <div class="active">

            <form class="block" method="post" action="/admin-panel/federation/{{$federation->site}}/update-logo" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="input-group">
                    <label class="input-group__title"> Логотип</label>
                    <div class="col-sm-4">
                        <img class="article-image" src="{{$federation->logo}}" width="100" height="100">
                    </div>
                </div>

                <div class="input-group">
                    <label class="file-input"> <span class="file-selected">Прикрепить Фото</span>
                        <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                        <input type="file" style="display:none;" class="fileUpload" name="logo">
                    </label>
                </div>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>
            </form>

            <form class="block" method="post" action="/admin-panel/federation/{{$federation->site}}/update-image" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="input-group">
                    <label class="input-group__title"> Фото</label>
                    <div class="col-sm-4">
                        <img class="article-image" src="{{$federation->image}}" width="200" height="200">
                    </div>
                </div>

                <div class="input-group">
                    <label class="file-input"> <span class="file-selected">Прикрепить Фото</span>
                        <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                        <input type="file" style="display:none;" class="fileUpload" name="image">
                    </label>
                </div>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>
            </form>

            <form class="block" method="post" action="/admin-panel/federation/{{$federation->site}}" enctype="multipart/form-data">
                @csrf
                @method('put')

                <br>
                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="name" value="{{$federation->name}}" placeholder="Название" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Название спорта</label>
                    <input type="text" name="sport" value="{{$federation->sport}}" placeholder="Название спорта" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Номер телефона</label>
                    <input type="text" name="phone" value="{{$federation->phone}}" placeholder="Номер телефона" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Почта</label>
                    <input type="email" name="email" value="{{$federation->email}}" placeholder="Почта" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Факс</label>
                    <input type="text" name="fax" value="{{$federation->fax}}" placeholder="Факс" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Веб-сайт</label>
                    <input type="text" name="website" value="{{$federation->website}}" placeholder="Веб-сайт" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Адрес</label>
                    <input type="text" name="address" value="{{$federation->address}}" placeholder="Адрес" class="input-regular" required>
                </div>
                <input type="hidden" name="site" value="{{$federation->site}}" placeholder="Ключевое слово" class="input-regular">

                <hr>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>

                @if ($errors->{ $bag ?? 'default' }->any())
                    <ul class="field mt-6 list-reset">
                        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                            <li class="sm:text-xs text-red">{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </form>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
