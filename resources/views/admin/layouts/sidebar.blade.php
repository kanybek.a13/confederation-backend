    <aside class="sidebar">
    <div class="sidebar__top hidden-sm hidden-xs">
        <a href="/" title="Главная" class="logo"><img src="/assets/img/admin-panel-logo.svg" alt=""></a>
    </div>
    <div class="menu-wrapper">
        <ul class="menu">
            <li class="dropdown">
                <a href="javascript:;" title="Заявка"><i class="icon-organizations"></i>Index</a>
                <ul>
                    <li><a href="/admin-panel" title="Список заявок">Admin index</a></li>
                </ul>
                <ul>
                    <li><a href="/" title="Список заявок">App index</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Пользователи"><i class="icon-archive"></i> Пользователи</a>
                <ul>
                    <li><a href="/admin-panel/users" title="Список пользователей">Список пользователей</a></li>
                    <li><a href="/admin-panel/create/user" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Федерации"><i class="icon-archive"></i> Федерации</a>
                <ul>
                    <li><a href="/admin-panel/federations" title="Список пользователей">Список федерации</a></li>
                    <li><a href="/admin-panel/federation/create" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li>

            @forelse($allFederations as $federation)
                <li class="dropdown">
                    <a href="javascript:;" title=" Секции"><i class="icon-archive"></i> {{$federation->name}}</a>
                    <ul>
                        <li class="dropdown">
                            <a href="javascript:;" title="Новости"><i class="icon-organizations"></i>О Федерациии</a>
                            <ul>
                                <li class="dropdown">
                                    <a href="javascript:;" title="Руководство"><i class="icon-organizations"></i>Руководство</a>
                                    <ul>
                                        <li><a href="/admin-panel/{{$federation->site}}/executive-committees" title="Список новостей"> Список руководителей</a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="/admin-panel/{{$federation->site}}/executive-committee/create" title="Добавить" class="add">+Добавить</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul>
                                <li class="dropdown">
                                    <a href="javascript:;" title="Результаты"><i class="icon-organizations"></i>Результаты</a>
                                    <ul>
                                        <li><a href="/admin-panel/{{$federation->site}}/results" title="Список новостей"> Список Результат</a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="/admin-panel/{{$federation->site}}/result/create" title="Добавить" class="add">+Добавить</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/structure" title="Cтруктура"> Cтруктура</a></li>
                            </ul>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/apparatus" title="Аппарат федерации"> Аппарат федерации</a></li>
                            </ul>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/history" title="История"> История</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="javascript:;" title="Новости"><i class="icon-organizations"></i>Новости</a>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/news" title="Список новостей"> Список новостей</a></li>
                            </ul>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/news/create" title="Добавить" class="add">+Добавить</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="javascript:;" title="Медиа"><i class="icon-organizations"></i>Медиа</a>

                            <ul>
                                <li><a href="{{route('admin.medias', [$federation->site])}}" title="Список медиа"> Список Meдиа</a></li>
                            </ul>
                            <ul>
                                <li><a href="{{route('admin.media.create', [$federation->site, 'type'=>'photo'])}}" title="Добавить Фото" class="add"> + Добавить Фото</a></li>
                            </ul>
                            <ul>
                                <li><a href="{{route('admin.media.create', [$federation->site, 'type'=>'video'])}}" title="Добавить Видео" class="add"> + Добавить Видео</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="javascript:;" title="Календарь"><i class="icon-organizations"></i>Календарь</a>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/events" title="Календарь событий"> Календарь событий</a></li>
                            </ul>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/event/create" title="Добавить" class="add">+Добавить</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="javascript:;" title="Сборные команды"><i class="icon-organizations"></i>Сборные команды</a>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/teams" title="Список команд"> Список команд</a></li>
                            </ul>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/team/create" title="Добавить" class="add">+Добавить</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="javascript:;" title="Атлеты"><i class="icon-organizations"></i>Атлеты</a>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/athletes" title="Список команд"> Список атлетов</a></li>
                            </ul>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/athlete/create" title="Добавить" class="add">+Добавить</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="javascript:;" title="Тренеры"><i class="icon-organizations"></i> Тренеры</a>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/coaches" title="Список тренеров"> Список тренеров</a></li>
                            </ul>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/coach/create" title="Добавить" class="add">+Добавить</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="javascript:;" title="Секции"><i class="icon-organizations"></i>Секции</a>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/sections" title="Список секции"> Список секции</a></li>
                            </ul>
                            <ul>
                                <li><a href="/admin-panel/{{$federation->site}}/section/create" title="Добавить" class="add">+Добавить</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</aside>
