
</main>

<footer class="footer">
</footer>
</div></div>
<script src="/admin/libs/jquery/dist/jquery.js"></script>
<script src="/admin/libs/maskedinput/maskedinput.js"></script>
<script src="/admin/libs/fancybox/dist/jquery.fancybox.min.js"></script>
<script src="/admin/libs/chosen/chosen.jquery.js"></script>
<script src="/admin/libs/air-datepicker/dist/js/datepicker.js"></script>
<script type="text/javascript" src="/admin/libs/plupload/js/plupload.full.min.js"></script>
<script src="/admin/js/scripts.js"></script>


<!--Only this page's scripts-->
    @yield('content')
<!---->


<div id="message" class="modal" style="display: none;">
    <h4 class="title-secondary">Удаление</h4>
    <div class="plain-text">
        При удалении все данные будут удалены
    </div>
    <hr>
    <div class="buttons justify-end">
        <div><button class="btn btn--red">Удалить</button></div>
        <div><button class="btn" data-fancybox-close>Отмена</button></div>
    </div>
</div>

<!--
<script>
    $.fancybox.open({
      src: "#message",
      touch: false
    })
</script>-->


</body>
</html>
