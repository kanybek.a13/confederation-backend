<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Flowdoc</title>

    <link rel="stylesheet" href="/admin/libs/fancybox/dist/jquery.fancybox.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/libs/chosen/chosen.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/libs/air-datepicker/dist/css/datepicker.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/css/style.css" type="text/css" media="screen"/>

    <link rel="apple-touch-icon" sizes="180x180" href="/admin/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/admin/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/admin/favicon/favicon-16x16.png">
    <link rel="manifest" href="/admin/favicon/site.webmanifest">
    <link rel="mask-icon" href="/admin/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <meta name="title" content="Заголовок">
    <meta name="description" content="Описание">
    <meta name="keywords" content="">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Заголовок">
    <meta property="og:description" content="Описание">
    <meta property="og:url" content="/">
    <meta property="og:image" content="/admin/img/og-image.jpg">
    <meta property="og:site_name" content="localhost:9876">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Заголовок">
    <meta property="twitter:description" content="Описание">
    <meta property="twitter:image" content="/admin/img/og-image.jpg">
    <meta property="twitter:site" content="">
    <meta property="twitter:creator" content="">
    <meta property="twitter:url" content="/">
    <meta property="author" content="">
    <meta name="relap-image" content="/admin/img/og-image.jpg">
    <meta name="relap-title" content="Заголовок">
    <meta name="relap-description" content=" ">
</head>
<body>

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">

    @include('admin.layouts.sidebar')

    <div class="right-wrapper">
        <header class="header">
            <div class="container container-fluid">
                <a href="javascript:;" title="Свернуть/развернуть навигацию" class="menu-btn icon-menu"></a>
                <a href="/" title="Главная" class="logo hidden-md hidden-lg"><img src="/admin/img/logo-blue.svg" alt=""></a>
                <div class="language hidden-sm hidden-xs">
                    <a href="#" title="РУС" class="active">РУС</a>
                    <a href="#" title="ENG">ENG</a>
                </div>
                <div class="header-dropdown account-nav">
                    <div class="header-dropdown__title">
{{--                        <span>Добро пожаловать, Admin!</span> <img src="/admin/img/user.svg" alt=""> <i--}}
                        <span>Добро пожаловать, {{auth()->user()->name}}!</span> <img src="/admin/img/user.svg" alt=""> <i
                            class="icon-chevron-down"></i>
                    </div>
                    <div class="header-dropdown__desc">
                        <ul>
                            <li class="language hidden-md hidden-lg"><a href="#" title="РУС" class="active">РУС</a><a href="#" title="ENG">ENG</a></li>
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    Выйти
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <main class="main">
