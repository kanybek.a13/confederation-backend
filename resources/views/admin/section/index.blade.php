@include('admin.layouts.header')

<main class="main">
    <div class="container container-fluid">
        <br>
        <ul class="breadcrumbs">
            <li><span>{{$currentFederation->name}}</span></li>
            <li><a href="{{ route('admin.sections', [$currentFederation->site]) }}" title="Секции">Секции</a></li>
        </ul>
    </div>
    <div class="container ">
        <section>
            <h2 class="title-primary"> Секции</h2>

            <form action="{{ route('admin.sections', [$currentFederation->site]) }}" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Название</label>
                            <input type="text" name="name" class="input-regular" placeholder="Название"  value="{{request('name')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Секции</h2>

            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Телефон</th>
                    <th>Город</th>
                    <th>Адрес</th>
                    <th>Федерация</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                @forelse($allSections as $section)
                    <tr>
                        <td>{{$section->id}}</td>
                        <td>{{$section->name}}</td>
                        <td>{{$section->phone}}</td>
                        <td>{{$section->city->name}}</td>
                        <td>{{$section->address}}</td>
                        <td>{{$section->federation->name}}</td>
                        <td>
                            <div class="action-buttons">
                                <a href="{{ route('admin.section.show', [$currentFederation->site, $section->id]) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                <a href="{{ route('admin.section.edit', [$currentFederation->site, $section->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                <form action="{{ route('admin.section.delete', [$currentFederation->site, $section->id ] )}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="text-align:center">No members yet.</div>
                @endforelse
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$allSections->currentPage() === 1 ? 'disabled' : '' }} href="{{ route('admin.sections', [$currentFederation->site]) }}?page={{ $allSections->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $allSections->lastPage(); $page++)
                    <li><a {{ $page === $allSections->currentPage() ? 'class="active"' : '' }} href="{{ route('admin.sections', [$currentFederation->site]) }}?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$allSections->currentPage() === $allSections->lastPage() ? 'disabled' : '' }} href="{{ route('admin.sections', [$currentFederation->site]) }}?page={{ $allSections->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
        <!---->
@endsection
