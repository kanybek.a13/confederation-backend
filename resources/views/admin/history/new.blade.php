@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.history', [$currentFederation->site]) }}" title="History">History</a></li>
        <li><span>New</span></li>
    </ul>

    <form class="block" method="post" action="{{ route('admin.history.store', [$currentFederation->site]) }}" enctype="multipart/form-data">
        @csrf
        @method('post')

        <div class="input-group">
            <label class="input-group__title"> Title</label>
            <input type="text" name="title" value="{{ old('title') }}" placeholder="Title" class="input-regular">
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title"> Body</label>
            <textarea type="text" name="body" class="input-regular" placeholder="Body" required>{{ old('body') }}</textarea>
        </div>
        <br>
        <div class="input-group">
            <label class="file-input"> <span class="file-selected">Прикрепить Фото</span>
                <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                <input type="file" style="display:none;" class="fileUpload" name="image" >
            </label>
        </div>
        <br>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>

        @if ($errors->{ $bag ?? 'default' }->any())
            <ul class="field mt-6 list-reset">
                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                    <li class="sm:text-xs text-red">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </form>
</div>
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
