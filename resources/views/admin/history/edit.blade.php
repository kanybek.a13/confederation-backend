@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.history', [$currentFederation->site]) }}" title="{{$history->title}}">History</a></li>
        <li><span>Edit</span></li>
    </ul>
    <div class="tabs-contents">
        <div class="active">
            <form class="block" method="post" action="{{ route('admin.history.update', [$currentFederation->site]) }}">
                @csrf
                @method('put')

                <div class="input-group">
                    <label class="input-group__title"> Title</label>
                    <input type="text" name="title" value="{{$history->title}}" placeholder="Title" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Body</label>
                    <textarea type="text" name="body" class="input-regular" placeholder="Body" required>{{$history->body}}</textarea>
                </div>
                <br>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>

                @if ($errors->{ $bag ?? 'default' }->any())
                    <ul class="field mt-6 list-reset">
                        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                            <li class="sm:text-xs text-red">{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </form>
        </div>
    </div>
</div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
