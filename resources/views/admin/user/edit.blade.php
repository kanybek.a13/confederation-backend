@include('admin.layouts.header')
@can('admin')

@if ($errors->any())
    <ul class="field mt-6 list-reset">
        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
            <li class="sm:text-xs text-red">{{ $error }}</li>
        @endforeach
    </ul>
@endif

<div class="container container-fluid">
                <br>
                <ul class="breadcrumbs">
                    <li><a href="/admin/users" title="Доктор">Пользователи</a></li>
                    <li><span>{{$user->name}}</span></li>
                </ul>

                <form class="block" method="post" action="/admin/user/{{$user->id}}">
                    @csrf
                    @method('put')

                    <div class="tabs-contents">
                        <div class="active">
                            <div class="input-group">
                                <label class="input-group__title"> Фамилия</label>
                                <input type="text" name="surname" value="{{$user->surname}}" class="input-regular" placeholder="Фамилия" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Имя</label>
                                <input type="text" name="name" value="{{$user->name}}" placeholder="Имя" class="input-regular">
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Отчества</label>
                                <input type="text" name="patronymic" value="{{$user->patronymic}}" class="input-regular" placeholder="Отчество" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> ИИН</label>
                                <input type="text" name="iin" class="input-regular" value="{{$user->iin}}" placeholder="ИИН" maxlength="12" minlength="12" data-validate="iin" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Номер телефона</label>
                                <input type="text" name="phone" class="input-regular" value="{{$user->phone}}" placeholder="Номер телефона" data-validate="phone" onfocus="$(this).inputmask('+7 999 999 99 99')" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Почта</label>
                                <input type="text" name="email" class="input-regular" value="{{$user->email}}" data-validate="email" placeholder="E-mail" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Пароль</label>
                                <input type="password" name="password" class="input-regular" placeholder="Введите пароль" data-validate="password" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Подтвердите Пароль</label>
                                <input type="password" name="password_confirmation" class="input-regular" placeholder="Подтвердите пароль" data-validate="password_confirmation" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Role</label>
                                <select name="role" class="input-regular chosen" data-placeholder="Role">
                                    <option value=""></option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->name}}" {{$role->name === $user->roles[0]->name ? 'selected' : ''}}>
                                            {{$role->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                        </div>
                    </div>

                    <hr>
                    <div class="buttons">
                        <div>
                            <button type="submit" class="btn btn--green">Сохранить</button>
                        </div>
                        <div>
                            <button class="btn btn--red"><a href="/admin/user/delete/{{$user->id}}" title="Удалить"></a>Удалить</button>
                        </div>
                    </div>
                </form>
            </div>

@endcan
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
