@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.results', [$currentFederation->site]) }}" title="Результаты">Результаты</a></li>
        <li><span>{{$result->name}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$result->id}}</div>
            <h1 class="fund-header__title">{{$result->name}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{$result->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения </div>
                <div class="property__text">{{$result->updated_at}}<br></div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                    </ul>
                    <div class="input-group">
                        <div class="row row--multiline">
                            <div class="col-md-4 col-sm-6">
                                <a href="{{ route('admin.result.edit', [$currentFederation->site, $result->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit" align="right" ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title"> Название</label>
                        <input type="text" name="name" value="{{$result->name}}" placeholder="Название" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <a href="{{$result->path}}" download="" class="card-regular" target="_blank">
                            <img class="icon" src="/assets/img/file.png">
                            <div class="d-flex align-items-center download-details">
                                <i class="flaticon-down"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
        <!---->
@endsection
