@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.results', [$currentFederation->site]) }}" title="Результаты">Результаты</a></li>
        <li><span>New</span></li>
    </ul>

    <form class="block" method="post" action="{{ route('admin.result.store', [$currentFederation->site]) }}" enctype="multipart/form-data">
        @csrf
        @method('post')

        <div class="input-group">
            <label class="input-group__title"> Название</label>
            <input type="text" name="name" value="{{ old('name')}}" placeholder="Название" class="input-regular" required>
        </div>
        <br>
        <div class="input-group">
            <label class="file-input"> <span class="file-selected">Прикрепить файл</span>
                <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                <input type="file" style="display:none;" class="fileUpload" name="file" required>
            </label>
        </div>
        <br>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>

        @if ($errors->{ $bag ?? 'default' }->any())
            <ul class="field mt-6 list-reset">
                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                    <li class="sm:text-xs text-red">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </form>
</div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
