@extends('layouts.header-start')
@section('content')

<section class="start-page">
        <a href="/confederation">
            <img src="/assets/img/flag.png">
        </a>
        <div class="container text-center">
        <h1>Конфедерация спортивных единоборств и силовых видов спорта</h1>
        <div class="sports">
            <div class="row">
                @forelse($federations as $federation)
                    @if($federation->site!=='confederation')
                        <div>
                            <a href="{{$federation->site}}" class="item">
                                <div class="img"><div><img src="{{$federation->logo}}"></div></div>
                                <h3>{{$federation->name}}</h3>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $('.start-page').css('min-height', $(window).height() - $('header').outerHeight() - $('footer').outerHeight());
    });
</script>

@include('layouts.footer-start')
@endsection
