@extends('layouts.app')
@section('main')

    <div class="main-wrapper">
        <section class="news">
            <div class="container">
                <div class="white-content">
                    <div class="title-breadcrumbs">
                        <h2>Календарь событий</h2>
                        <ul class="breadcrumbs">
                            <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                            <li><span>Календарь событий</span></li>
                        </ul>
                    </div>

                    <div class="list row">
                        @forelse($events as $event)
                            <div class="col-lg-6">
                                <a href="/{{$currentFederation->site}}/event/{{$event->id}}" class="d-flex align-items-center">
                                    <img src="{{$event->image}}">
                                    <div>
                                        <h3>{{$event->name}}</h3>
                                        <p class="meta">{{$event->start_date}}
                                            - {{$event->finish_date}}</p>
                                    </div>
                                </a>
                            </div>
                        @empty
                            No events yet!
                        @endforelse
                    </div>

                    <ul class="pagination">
                        <li class="previous_page disabled"><a href="#"><i class="fas fa-long-arrow-alt-left"></i></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="http://confederation.kz/ru/judo/events?page=2">2</a></li>
                        <li><a href="http://confederation.kz/ru/judo/events?page=3">3</a></li>
                        <li><a href="http://confederation.kz/ru/judo/events?page=4">4</a></li>
                        <li><a href="http://confederation.kz/ru/judo/events?page=5">5</a></li>
                        <li><a href="http://confederation.kz/ru/judo/events?page=6">6</a></li>
                        <li><a href="http://confederation.kz/ru/judo/events?page=7">7</a></li>
                        <li class="next_page"><a href="http://confederation.kz/ru/judo/events?page=2"><i class="fas fa-long-arrow-alt-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </section>

@endsection
