<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'path', 'federation_id'];

    public function federation(){
        return $this->belongsTo(Federation::class, 'federation_id', 'id');
    }
}
