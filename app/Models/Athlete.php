<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Athlete extends Model
{
    use HasFactory;

    protected $fillable = ['fullname', 'date_of_birth', 'birthplace', 'weight', 'rating', 'federation_id', 'about', 'image'];

    public function federation(){
        return $this->belongsTo(Federation::class, 'federation_id', 'id');
    }

    public function team(){
        return $this->belongsToMany(Team::class, 'athlete_team');
    }
}
