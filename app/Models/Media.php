<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'type', 'image', 'federation_id'];

    public function comment(){
        return $this->hasMany(MediaComments::class, 'media_id', 'id');
    }

    public function photos(){
        return $this->hasMany(Photo::class, 'media_id', 'id');
    }

    public function video(){
        return $this->hasOne(Video::class, 'media_id', 'id');
    }

    public function federation(){
        return $this->belongsTo(Federation::class, 'federation_id', 'id');
    }
}
