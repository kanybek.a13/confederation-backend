<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coach extends Model
{
    use HasFactory;

    protected $fillable = ['fullname', 'position', 'federation_id', 'image'];

    public function federation()
    {
        return $this->belongsTo(Federation::class);
    }

    public function teams()
    {
        return $this->belongsToMany( Team::class,'team_coach');
    }
}
