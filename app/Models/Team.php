<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug', 'federation_id'];

    public function athletes(){
        return $this->belongsToMany(Athlete::class, 'athlete_team');
    }

    public function coaches(){
        return $this->belongsToMany(Coach::class, 'team_coach');
    }

    public function federation()
    {
        return $this->belongsTo(Federation::class);
    }

    public function inviteAthlete($athlete)
    {
        $this->athletes()->attach($athlete->id);
    }
}
