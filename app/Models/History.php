<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'body', 'federation_id', 'image'];

    public function federation(){
        return $this->belongsTo(Federation::class, 'federation_id', 'id');
    }
}
