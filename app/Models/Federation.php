<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Federation extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'site', 'website', 'phone', 'email', 'fax', 'address', 'logo', 'image', 'sport'
    ];

    public function getRouteKeyName()
    {
        return 'site';
    }

    public function news(){
        return $this->hasMany(News::class, 'federation_id', 'id');
    }

    public function history(){
        return $this->hasOne(History::class, 'federation_id', 'id');
    }

    public function athletes(){
        return $this->hasMany(Athlete::class, 'federation_id', 'id');
    }

    public function executiveCommittee(){
        return $this->hasMany(ExecutiveCommittee::class, 'federation_id', 'id');
    }

    public function apparatus(){
        return $this->hasMany(Apparatus::class, 'federation_id', 'id');
    }

    public function results(){
        return $this->hasMany(Result::class, 'federation_id', 'id');
    }

    public function media(){
        return $this->hasMany(Media::class, 'federation_id', 'id');
    }

    public function teams(){
        return $this->hasMany(Team::class, 'federation_id', 'id');
    }

    public function coaches(){
        return $this->hasMany(Coach::class, 'federation_id', 'id');
    }

    public function athlete(){
        return $this->hasMany(Athlete::class, 'federation_id', 'id');
    }

    public function events(){
        return $this->hasMany(Event::class, 'federation_id', 'id');
    }
}
