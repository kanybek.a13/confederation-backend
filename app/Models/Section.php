<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'address', 'phone', 'image', 'city_id', 'federation_id'];

    public function federation()
    {
        return $this->belongsTo(Federation::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
