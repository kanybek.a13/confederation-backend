<?php

namespace App\Http\Controllers;

use App\Models\Federation;
use App\Models\News;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        return view('home');
    }

    public function start()
    {
        return view('start',[
            'federations' => Federation::all()
        ]);
    }

    public function adminPanel()
    {
        return view('admin.index');
    }

    public function adminLogin()
    {
        return view('admin.login');
    }


    public function confederation()
    {
        $last_news = News::take(6)->latest('created_at')->get();

        return view('index', compact('last_news'));
    }
}
