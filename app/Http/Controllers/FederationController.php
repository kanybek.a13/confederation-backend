<?php

namespace App\Http\Controllers;

use App\Models\Athlete;
use App\Models\City;
use App\Models\Coach;
use App\Models\Event;
use App\Models\Federation;
use App\Models\Media;
use App\Models\News;
use App\Models\Partner;
use App\Models\Section;
use App\Models\Sity;
use App\Models\Sport;
use App\Models\Team;
use App\Models\TeamMember;
use Illuminate\Http\Request;

class FederationController extends Controller
{
    public function index(Federation $federation)
    {
        return view('index', [
            'currentFederation' => $federation,
            'last_news' => $federation->news()
                ->latest('created_at')
                ->take(7)
                ->get(),
        ]);
    }

    public function news(Federation $federation)
    {
        return view('news.index',  [
            'currentFederation' => $federation,
            'last_news' => $federation->news()
                ->latest('created_at')
                ->paginate(10)
        ]);
    }

    public function newsShow(Federation $federation, News $news)
    {
        return view('news.show',  [
            'currentFederation' => $federation,
            'news' => $news
        ]);
    }

    public function executiveCommunity(Federation $federation)
    {
        return view('executive-committee', [
            'currentFederation' => $federation,
            'executiveCommittee' => $federation->executiveCommittee()->get()
        ]);
    }

    public function apparatus(Federation $federation)
    {
        $apparatuses = $federation->apparatus()->get();

        return view('apparatus', [
            'currentFederation' => $federation,
            'apparatuses' => $apparatuses,
        ]);
    }

    public function history(Federation $federation)
    {
        return view('history',[
            'currentFederation' => $federation,
            'history' => $federation->history()->get()->first()
        ]);
    }

    public function media(Federation $federation)
    {
        $medias = $federation->media()->get();

        return view('media.index', [
            'currentFederation' => $federation,
            'medias' => $medias
        ]);
    }

    public function mediaShow(Federation $federation, Media $media)
    {
        return view('media.photo-gallery', [
            'currentFederation' => $federation,
            'media' => $media,
            'photos' => $media->photos
        ]);
    }

    public function goals(Federation $federation)
    {
        return view('goals', [
            'currentFederation' => $federation,
        ]);
    }

    public function article(Federation $federation)
    {
        return view('news.article', [
            'currentFederation' => $federation,
        ]);
    }

    public function coaches(Federation $federation)
    {
        $teams = $federation->teams;

        if (\request('team_id')){
            $team = Team::where('id', \request('team_id'))->first();

            $coaches= $team->coaches;
        }else
            $coaches = $teams[0]->coaches;

        return view('coach.index', [
            'currentFederation' => $federation,
            'teams' => $teams,
            'coaches' => $coaches
        ]);
    }

    public function coachShow(Federation $federation, Coach $coach)
    {
        return view('coach.show', [
            'currentFederation' => $federation,
            'coach' => $coach
        ]);
    }

    public function interview(Federation $federation)
    {
        return view('interview', [
            'currentFederation' => $federation,
        ]);
    }

    public function massmedia(Federation $federation)
    {
        return view('media.massmedia', [
            'currentFederation' => $federation,
        ]);
    }

    public function events(Federation $federation)
    {
        return view('event.index', [
            'currentFederation' => $federation,
            'events' => $federation->events
        ]);
    }

    public function eventShow(Federation $federation, Event $event)
    {
        return view('event.show', [
            'currentFederation' => $federation,
            'event' => $event
        ]);
    }

    public function results(Federation $federation)
    {
        return view('results', [
            'currentFederation' => $federation,
            'results' => $federation->results,
        ]);
    }

    public function teams(Federation $federation)
    {
        $teams = $federation->teams;

        if (\request('team_id')){
            $team = Team::where('id', \request('team_id'))->first();

            $teamAthletes = $team->athletes;
        }else
            $teamAthletes = $teams[0]->athletes;

        return view('teams', [
            'currentFederation' => $federation,
            'teams' => $teams,
            'teamAthletes' => $teamAthletes
        ]);
    }

    public function athlete(Federation $federation, Team $team, Athlete $athlete)
    {
        return view('athlete', [
            'currentFederation' => $federation,
            'team' => $team,
            'athlete' => $athlete
        ]);
    }

    public function structure(Federation $federation)
    {
        return view('structure', [
            'currentFederation' => $federation,
            'structure' => 'structure',
        ]);
    }

    public function searchResult(Request $request, Federation $federation)
    {
        return view('search-results', [
            'currentFederation' => $federation,
        ]);
    }

    public function sections(Federation $federation, Request $request)
    {
        $federation_id = $request->input('federation_id');

        if ($federation_id && $federation_id != 'all'){
            $sections = Section::where('federation_id', $federation_id);
        }elseif ($federation_id === 'all'){
           $sections = Section::whereNotNull('federation_id');
        }else
            $sections = Section::where('federation_id', $federation->id);

        if ($name = $request->input('name')){
            $sections = $sections->where('name', 'LIKE', "%{$name}%");
        }

        if ($address = $request->input('address')){
            $sections = $sections->where('address', 'LIKE', "%{$address}%");
        }

        if ($city_id = $request->input('city_id')){
            $sections = $sections->where('city_id', $city_id);
        }

        $sections = $sections->paginate(10);

        return view('section.index', [
            'currentFederation' => $federation,
            'cities' => City::all()->sortBy('name'),
            'sections' => $sections
        ]);
    }

    public function sectionShow(Federation $federation, Section $section)
    {
        return view('section.show', [
            'currentFederation' => $federation,
            'section' => $section
        ]);
    }
}
