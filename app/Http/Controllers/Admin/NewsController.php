<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Federation;
use App\Models\File;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(Federation $federation)
    {
        $allNews = News::where('federation_id', $federation->id);

        if (\request('search')!=''){
            $allNews = $allNews->where('title', 'ILIKE' , '%'. \request('search') . '%')
                ->orWhere('body', 'ILIKE' , '%'. \request('search') . '%');
        }

        $allNews = $allNews->orderBy('id')
            ->paginate(10);

        return view('admin.news.index', [
            'allNews' => $allNews,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.news.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateNewsInfo($request);
        $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('image')) {

            $news= News::create([
                'title' => $attributes['title'],
                'body' => $attributes['body'],
                'federation_id' => $federation->id,
            ]);

            foreach ($request->file('image') as $image)

                $path = public_path(). '/uploads/';
                $filename = $image->GetClientOriginalName();

                $image->move($path, $filename);

                $file = File::create([
                    'name' => $filename,
                    'path' => '/uploads/' . $filename,
                ]);

                $news->assignFile($file);

            return redirect('/admin-panel/'. $federation->site .'/news');
        }

        return redirect()->back();
    }

    public function show(Federation $federation, News $news)
    {
        return view('admin.news.show', [
            'news' => $news,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation, News $news)
    {
        return view('admin.news.edit', [
            'news' => $news,
            'currentFederation' => $federation,
        ]);
    }

        public function update(Federation $federation, News $news)
    {
        $attributes = $this->validateNewsInfo(\request());

        $news->update([
            'title' => $attributes['title'],
            'body' => $attributes['body'],
        ]);

        return redirect('/admin-panel/'. $federation->site .'/news');
    }

    public function destroy(Federation $federation, News $news)
    {
        $news->delete();

        return redirect()->back();
    }

    public function validateNewsInfo($data){
        return request()->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
    }
}
