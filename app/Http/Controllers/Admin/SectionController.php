<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Federation;
use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function index(Federation $federation)
    {
        $allSections = Section::where('federation_id', $federation->id);

        if (\request('name')!=''){
            $allSections = $allSections->where('name', 'ILIKE' , '%'. \request('name') . '%');
        }

        $allSections = $allSections->orderBy('id')
            ->paginate(10);

        return view('admin.section.index', [
            'allSections' => $allSections,
            'cities' => City::all(),
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.section.new', [
            'cities' => City::all(),
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateSectionInfo($request);
        $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
//?            foreach ($request->file('image') as $image)

            $path = public_path(). '/uploads/';
            $filename = $image->GetClientOriginalName();

            $image->move($path, $filename);

            $section= Section::create([
                'name' => $attributes['name'],
                'phone' => $attributes['phone'],
                'address' => $attributes['address'],
                'city_id' => $attributes['city_id'],
                'federation_id' => $attributes['federation_id'],
                'image' => '/uploads/' . $filename,
            ]);

            return redirect(route('admin.section.show', [$federation->site, $section->id]));
        }

        return redirect()->back();
    }

    public function show(Federation $federation, Section $section)
    {
        return view('admin.section.show', [
            'section' => $section,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation, Section $section)
    {
        return view('admin.section.edit', [
            'section' => $section,
            'cities' => City::all(),
            'currentFederation' => $federation,
        ]);
    }

    public function update(Federation $federation, Section $section)
    {
        $attributes = $this->validateSectionInfo(\request());

        $section->update([
            'name' => $attributes['name'],
            'city_id' => $attributes['city_id'],
            'address' => $attributes['address'],
            'phone' => $attributes['phone'],
            'federation_id' => $attributes['federation_id'],
        ]);

        return redirect(route('admin.section.show', [$federation->site, $section->id]));
    }

    public function destroy(Federation $federation, Section $section)
    {
        $section->delete();

        return redirect()->back();
    }

    public function validateSectionInfo($data){
        return request()->validate([
            'name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|string',
            'city_id' => 'required|integer',
            'federation_id' => 'required|integer',
        ]);
    }
}
