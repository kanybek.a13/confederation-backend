<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Federation;
use App\Models\Media;
use Database\Seeders\PhotoSeeder;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    public function index(Federation $federation)
    {
        $mediaPhotos = Media::where('federation_id', $federation->id)
            ->where('type', 'photo')
            ->latest('created_at')
            ->paginate(10);

        return view('admin.photo.index', [
            'mediaPhotos' => $mediaPhotos,
            'currentFederation' => $federation,
        ]);
    }

    public function create()
    {
        return view('admin.news.new', [
            'federations' => Federation::all()
        ]);
    }

    public function store(Request $request)
    {
        $attributes = $this->validateMediaInfo($request);
        $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        $federation = Federation::where('site', $attributes['federation'])->first();

        if ($request->hasFile('image')) {

            $media= Media::create([
                'title' => $attributes['title'],
                'body' => $attributes['body'],
                'federation_id' => $federation->id,
            ]);

            foreach ($request->file('image') as $image)

                $path = public_path(). '/uploads/';
            $filename = $image->GetClientOriginalName();

            $image->move($path, $filename);

            $file = File::create([
                'name' => $filename,
                'path' => '/uploads/' . $filename,
            ]);

            $media->assignFile($file);

            return redirect('/admin-panel/media');
        }

        return redirect()->back();
    }

    public function show(Media $media)
    {
        dd($media);
        return view('admin.photo.show', compact('media'));
    }

    public function edit(Media $media)
    {
        return view('admin.news.edit', [
            'news' => $media,
            'federations' => Federation::all()
        ]);
    }

    public function update(Media $media)
    {
        $attributes = $this->validateMediaInfo(\request());
        $federation = Federation::where('site', $attributes['federation'])->first();

        $media->update([
            'title' => $attributes['title'],
            'body' => $attributes['body'],
            'federation_id' => $federation->id
        ]);

        return redirect('/admin-panel/media');
    }

    public function destroy(Media $media)
    {
        $media->delete();

        return redirect('/admin-panel/media');
    }

    public function validateMediaInfo($data){
        return request()->validate([
            'title' => 'required',
            'body' => 'required',
            'federation' => 'required',
        ]);
    }}
