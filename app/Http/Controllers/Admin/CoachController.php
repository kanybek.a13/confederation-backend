<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coach;
use App\Models\Federation;
use Illuminate\Http\Request;

class CoachController extends Controller
{
    public function index(Federation $federation)
    {
        $allCoaches = Coach::where('federation_id', $federation->id);

        if (\request('fullname')!=''){
            $allCoaches = $allCoaches->where('fullname', 'ILIKE' , '%'. \request('fullname') . '%');
        }

        $allCoaches = $allCoaches->orderBy('id')
            ->paginate(10);

        return view('admin.coach.index', [
            'allCoaches' => $allCoaches,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.coach.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateCoachInfo($request);
        $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $path = public_path(). '/uploads/';
            $filename = $image->GetClientOriginalName();

            $image->move($path, $filename);

            $coach= Coach::create([
                'fullname' => $attributes['fullname'],
                'position' => $attributes['position'],
                'federation_id' => $attributes['federation_id'],
                'image' => '/uploads/' . $filename
            ]);

            return redirect(route('admin.coach.show', [$federation->site, $coach->id]));
        }

        return redirect()->back();
    }

    public function show(Federation $federation, Coach $coach)
    {
        return view('admin.coach.show', [
            'coach'=> $coach,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation, Coach $coach)
    {
        return view('admin.coach.edit', [
            'coach' => $coach,
            'currentFederation' => $federation,
        ]);
    }

    public function update(Federation $federation, Coach $coach)
    {
        $attributes = $this->validateCoachInfo(\request());

        $coach->update([
            'fullname' => $attributes['fullname'],
            'position' => $attributes['position'],
            'federation_id' => $attributes['federation_id']
        ]);

        return redirect(route('admin.coach.show', [$federation->site, $coach->id]));
    }

    public function destroy(Federation $federation, Coach $coach)
    {
        $coach->delete();

        return redirect()->back();
    }

    public function validateCoachInfo($data){
        return request()->validate([
            'fullname' => 'required|string',
            'position' => 'required|string',
            'federation_id' => 'required|integer',
        ]);
    }
}
