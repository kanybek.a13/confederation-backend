<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Federation;
use App\Models\Result;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    public function index(Federation $federation)
    {
        $results = Result::where('federation_id', $federation->id);

        if (\request('name')!=''){
            $results = $results->where('fullname', 'ILIKE' , '%'. \request('name') . '%');
        }

        $results = $results->orderBy('id')
            ->paginate(10);

        return view('admin.result.index', [
            'results' => $results,
            'currentFederation' => $federation,
        ]);
    }

    public function show(Federation $federation, Result $result)
    {
        return view('admin.result.show', [
            'result'=> $result,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation, Result $result)
    {
        return view('admin.result.edit', [
            'result' => $result,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.result.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateResultsInfo($request);

        $request->validate([
            'file' => 'required|max:10000|mimes:doc,docx',
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file');

            $file_path = public_path() . '/assets/img/results/';

            $filename = $file->GetClientOriginalName();

            $file->move($file_path, $filename);

            $result = Result::create([
                'name' => $attributes['name'],
                'path' => $file_path . '/' . $filename,
                'federation_id' => $federation->id,
            ]);

            return redirect(route('admin.results', [$federation->site]));
        }

        return redirect()->back();
    }

    public function update(Federation $federation, Result $result)
    {
        $attributes = $this->validateResultsInfo(\request());

        $result->update($attributes);

        return redirect(route('admin.result.show', [$federation->site, $result->id]));
    }

    public function destroy(Federation $federation, Result $result)
    {
        $result->delete();

        return redirect(route('admin.results', [$federation->site]));
    }

    public function validateResultsInfo($data){
        return request()->validate([
            'name' => 'required'
        ]);
    }
}
