<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Athlete;
use App\Models\Federation;
use App\Models\Team;
use App\Models\TeamMember;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TeamController extends Controller
{
    public function index(Federation $federation)
    {
        $allTeams = Team::where('federation_id', $federation->id);

        if (\request('name')!=''){
            $allTeams = $allTeams->where('name', 'ILIKE' , '%'. \request('name') . '%');
        }

        $allTeams = $allTeams->orderBy('id')
            ->paginate(10);

        return view('admin.team.index', [
            'allTeams' => $allTeams,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.team.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateTeamInfo($request);

        $team= Team::create([
            'name' => $attributes['name'],
            'slug' => Str::slug($attributes['name']),
            'federation_id' => $attributes['federation_id'],
        ]);

        return redirect(route('admin.team.show', [$federation->site, $team->id]));
    }

    public function show(Federation $federation, Team $team)
    {
        return view('admin.team.show', [
            'team'=> $team,
            'currentFederation' => $federation,
            'athletes' => $team->athletes,
            'federationAthletes' => $federation->athletes
        ]);
    }

    public function edit(Federation $federation, Team $team)
    {
        return view('admin.team.edit', [
            'team' => $team,
            'currentFederation' => $federation,
        ]);
    }

    public function update(Federation $federation, Team $team)
    {
        $attributes = $this->validateTeamInfo(\request());

        $team->update([
            'name' => $attributes['name'],
            'slug' => Str::slug($attributes['name']),
            'federation_id' => $attributes['federation_id']
        ]);

        return redirect(route('admin.team.show', [$federation->site, $team->id]));
    }

    public function destroy(Federation $federation, Team $team)
    {
        $team->delete();

        return redirect()->back();
    }

    public function inviteAthlete(Federation $federation, Team $team)
    {
        $athlete = Athlete::find(\request('athlete_id'));

        if ($athlete){
            $team->athletes()->syncWithoutDetaching($athlete);
        }

        return redirect(route('admin.team.show', [$federation->site, $team->id]));
    }

    public function deleteAthlete(Federation $federation, Team $team, Athlete $athlete)
    {
        $team->athletes()->detach($athlete);

        return redirect(route('admin.team.show', [$federation->site, $team->id]));
    }

    public function validateTeamInfo($data){
        return request()->validate([
            'name' => 'required|string',
            'federation_id' => 'required|integer',
        ]);
    }
}
