<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Federation;
use App\Models\Media;
use App\Models\Photo;
use App\Models\Video;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function index(Federation $federation)
    {
        $allMedia = Media::where('federation_id', $federation->id);

        if (\request('name')!=''){
            $allMedia = $allMedia->where('name', 'ILIKE' , '%'. \request('name') . '%');
        }

        $allMedia = $allMedia->latest('created_at')
            ->paginate(10);

        return view('admin.media.index', [
            'allMedia' => $allMedia,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation, $type)
    {
        if ($type === 'video'){
            return view('admin.media.new-video', [
                'currentFederation' => $federation,
            ]);

        }elseif ($type === 'photo'){
            return view('admin.media.new-photo', [
                'currentFederation' => $federation,
            ]);
        }
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateMediaInfo($request);

        if ($attributes['type'] === 'video') {
            $request->validate([
                'source'=>'required',
                'image' => 'required',
            ]);

            if ($request->hasFile('image')) {
                $image = $request->file('image');

                $path = public_path() . '/uploads/';
                $filename = $image->GetClientOriginalName();

                $image->move($path, $filename);

                $media = Media::create([
                    'name' => $attributes['name'],
                    'type' => $attributes['type'],
                    'image' => '/uploads/' . $filename,
                    'federation_id' => $federation->id,
                ]);

                Video::create([
                    'name' => $attributes['name'],
                    'source' => $request['source'],
                    'media_id' => $media->id
                ]);

                return redirect(route('admin.medias', [$federation->site]));

            }
        }

        if ($attributes['type']==='photo'){
            $request->validate([
                'images' => 'required',
            ]);

            if ($request->hasFile('images')) {
                $media= Media::create([
                    'name' => $attributes['name'],
                    'type' => $attributes['type'],
                    'image' => '',
                    'federation_id' => $federation->id,
                ]);
                $images = $request->file('images');

                foreach($images as $image) {
                    $name = $image->getClientOriginalName();
                    $path = public_path() . '/uploads/';

                    $image->move($path, $name);

                    Photo::create([
                        'name' => $name,
                        'path' => '/uploads/' . $name,
                        'media_id' => $media->id
                    ]);
                }

                $media->update(['image' => $media->photos[0]['path']]);

                return redirect(route('admin.medias', [$federation->site]));
            }
        }

        return redirect()->back();
    }

    public function show(Media $media)
    {
        return view('admin.media.show', compact('media'));
    }

    public function edit(Federation $federation, Media $media)
    {
        return view('admin.media.edit-'. $media['type'], [
            'media' => $media,
            'currentFederation' => $federation,
        ]);
    }

    public function update(Request $request, Federation $federation, Media $media)
    {
        $attributes = $this->validateMediaInfo(\request());

        if ($attributes['type']==='photo'){
            $request->validate([
                'images' => 'required',
            ]);

            if ($request->hasFile('images')) {
                $media->update([
                    'name' => $attributes['name'],
                    'type' => $attributes['type'],
                    'image' => '',
                ]);

                $images = $request->file('images');

                foreach($images as $image) {
                    $name = $image->getClientOriginalName();
                    $path = public_path() . '/uploads/';

                    $image->move($path, $name);

                    Photo::create([
                        'name' => $name,
                        'path' => '/uploads/' . $name,
                        'media_id' => $media->id
                    ]);
                }

                $media->update(['image' => $media->photos[0]['path']]);

                return redirect(route('admin.medias', [$federation->site]));
            }
        }

        if ($attributes['type']==='video') {
            $media->update([
                'name' => $attributes['name'],
                'image' => $attributes['image'],
            ]);

            $media->video->update();
        }

        return redirect('/admin-panel/media');
    }

    public function destroy(Federation $federation, Media $media)
    {
        $media->delete();

        return redirect(route('admin.medias', [$federation->site]));
    }

    public function destroyPhoto(Federation $federation, Media $media, Photo $photo)
    {
        $photo->delete();

        return redirect(route('admin.media.edit', [$federation->site, $media->id]));
    }

    public function validateMediaInfo($data){
        return request()->validate([
            'name' => 'required',
            'type' => 'required',
        ]);
    }
}
