<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Federation;
use Illuminate\Http\Request;

class FederationController extends Controller
{
    public function index()
    {
        if (\request('name')!=''){
            $federations = Federation::where('name', 'ILIKE' , '%'. \request('name') . '%');
        }else
            $federations = Federation::whereNotNull('id');

        $federations = $federations->orderBy('id')
            ->paginate(10);

        return view('admin.federation.index', [
            'federations' => $federations,
        ]);
    }

    public function show(Federation $federation)
    {
        return view('admin.federation.show', [
            'federation'=> $federation,
        ]);
    }

    public function create()
    {
        return view('admin.federation.new', [
        ]);
    }

    public function edit(Federation $federation)
    {
        return view('admin.federation.edit', [
            'federation' => $federation,
        ]);
    }

    public function update(Federation $federation)
    {
        $attributes = $this->validateFederationInfo(\request());

        $federation->update($attributes);

        return redirect('/admin-panel/federations');
    }

    public function store(Request $request)
    {
        $attributes = $this->validateFederationInfo($request);
        $validatedImages = $request->validate([
            'logo' => 'required:jpg,jpeg,bmp,png',
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('logo') && $request->hasFile('image')) {
            $logo = $request->file('logo');
            $image = $request->file('image');

            $logo_path = public_path() . '/assets/img/sports/';
            $image_path = public_path() . '/assets/img/federations/';

            $logo_filename = $logo->GetClientOriginalName();
            $image_filename = $image->GetClientOriginalName();

            $logo->move($logo_path, $logo_filename);
            $image->move($image_path, $image_filename);

            $federation = Federation::create([
                'name' => $attributes['name'],
                'email' => $attributes['email'],
                'phone' => $attributes['phone'],
                'fax' => $attributes['fax'],
                'site' => $attributes['site'],
                'website' => $attributes['website'],
                'sport' => $attributes['sport'],
                'address' => $attributes['address'],
                'logo' => $logo_path . '/' . $logo_filename,
                'image' => $image_path . '/' . $image_filename,
            ]);

            return redirect('/admin-panel/federations');
        }

        return redirect()->back();
    }

    public function updateLogo(Federation $federation, Request $request)
    {
        $validatedImage = $request->validate([
            'logo' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');

            $path = public_path() . '/uploads/' . $federation['site'];
            $filename = $logo->GetClientOriginalName();

            $logo->move($path, $filename);

            $federation->update([
                'logo' => '/uploads/' . $federation['site'] . '/' . $filename,
            ]);

            return redirect('/admin-panel/federations');
        }

        return back();
    }

    public function updateImage(Federation $federation, Request $request)
    {
        $validatedImage = $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $path = public_path() . '/uploads/' . $federation['site'];
            $filename = $image->GetClientOriginalName();

            $image->move($path, $filename);

            $federation->update([
                'image' => '/uploads/' . $federation['site'] . '/' . $filename,
            ]);

            return redirect('/admin-panel/federations');
        }

        return back();
    }

    public function destroy()
    {
        $federation = Federation::findOrFail(\request('id'));
        $federation->delete();

        return redirect('/admin-panel/federations');
    }

    public function validateFederationInfo($data){
        return request()->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'fax' => 'required',
            'email' => 'required',
            'site' => 'required',
            'website' => 'required',
            'sport' => 'required',
        ]);
    }
}
