<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ExecutiveCommittee;
use App\Models\Federation;
use Illuminate\Http\Request;

class ExecutiveCommitteeController extends Controller
{
    public function index(Federation $federation)
    {
        $executiveCommittees = ExecutiveCommittee::where('federation_id', $federation->id);

        if (\request('fullname')!=''){
            $executiveCommittees = $executiveCommittees->where('fullname', 'ILIKE' , '%'. \request('fullname') . '%');
        }

        $executiveCommittees = $executiveCommittees->orderBy('id')
            ->paginate(10);

        return view('admin.executive-committee.index', [
            'executiveCommittees' => $executiveCommittees,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.executive-committee.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateInfo($request);
        $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $path = public_path(). '/uploads/';
            $filename = $image->GetClientOriginalName();

            $image->move($path, $filename);

            $executiveCommittee = ExecutiveCommittee::create([
                'fullname' => $attributes['fullname'],
                'position' => $attributes['position'],
                'federation_id' => $attributes['federation_id'],
                'image' => '/uploads/' . $filename
            ]);

            return redirect(route('admin.executive-committee.show', [$federation->site, $executiveCommittee->id]));
        }

        return redirect()->back();
    }

    public function show(Federation $federation, ExecutiveCommittee $executiveCommittee)
    {
        return view('admin.executive-committee.show', [
            'executiveCommittees'=> $executiveCommittee,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation, ExecutiveCommittee $executiveCommittee)
    {
        return view('admin.executive-committee.edit', [
            'executiveCommittees' => $executiveCommittee,
            'currentFederation' => $federation,
        ]);
    }

    public function update(Federation $federation, ExecutiveCommittee $executiveCommittee)
    {
        $attributes = $this->validateInfo(\request());

        $executiveCommittee->update([
            'fullname' => $attributes['fullname'],
            'position' => $attributes['position'],
            'federation_id' => $attributes['federation_id']
        ]);

        return redirect(route('admin.executive-committee.show', [$federation->site, $executiveCommittee->id]));
    }

    public function destroy(Federation $federation, ExecutiveCommittee $executiveCommittee)
    {
        $executiveCommittee->delete();

        return redirect()->back();
    }

    public function validateInfo($data){
        return request()->validate([
            'fullname' => 'required|string',
            'position' => 'required|string',
            'federation_id' => 'required|integer',
        ]);
    }
}
