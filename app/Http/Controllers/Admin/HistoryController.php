<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Federation;
use App\Models\History;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    public function index(Federation $federation)
    {
        $history = History::where('federation_id', $federation->id);

        return view('admin.history.index', [
            'history' => $history,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.history.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateInfo($request);
        $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $path = public_path(). '/uploads/';
            $filename = $image->GetClientOriginalName();

            $image->move($path, $filename);

            $history= History::create([
                'title' => $attributes['title'],
                'body' => $attributes['body'],
                'federation_id' => $federation->id,
                'image' => '/uploads/' . $filename
            ]);

            return redirect(route('admin.history', [$federation->site]));
        }

        return redirect()->back();
    }

    public function show(Federation $federation)
    {
        return view('admin.history.show', [
            'history' => $federation->history,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation)
    {
        return view('admin.history.edit', [
            'history' => $federation->history,
            'currentFederation' => $federation,
        ]);
    }

    public function update(Federation $federation)
    {
        $attributes = $this->validateInfo(\request());

        $federation->history->update([
            'title' => $attributes['title'],
            'body' => $attributes['body'],
            'federation_id' => $federation->id
        ]);

        return redirect(route('admin.history', [$federation->site]));
    }

    public function destroy(Federation $federation)
    {
        $federation->history->delete();

        return redirect(route('admin.history', [$federation->site]));
    }

    public function validateInfo($data){
        return request()->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
    }
}
