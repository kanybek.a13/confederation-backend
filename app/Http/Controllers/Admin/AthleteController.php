<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Athlete;
use App\Models\Federation;
use App\Models\Team;
use Illuminate\Http\Request;

class AthleteController extends Controller
{
    public function index(Federation $federation)
    {
        $athletes = Athlete::where('federation_id', $federation->id);

        if (\request('name')!=''){
            $athletes = $athletes->where('fullname', 'ILIKE' , '%'. \request('name') . '%');
        }

        $athletes = $athletes->orderBy('id')
            ->paginate(10);

        return view('admin.athlete.index', [
            'athletes' => $athletes,
            'currentFederation' => $federation,
        ]);
    }

    public function show(Federation $federation, Athlete $athlete)
    {
        return view('admin.athlete.show', [
            'athlete'=> $athlete,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation, Athlete $athlete)
    {
        return view('admin.athlete.edit', [
            'athlete' => $athlete,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.athlete.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateAthleteInfo($request);

        $validatedImages = $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $image_path = public_path() . '/assets/img/athletes/';

            $image_filename = $image->GetClientOriginalName();

            $image->move($image_path, $image_filename);

            $athlete = Athlete::create([
                'fullname' => $attributes['fullname'],
                'birthplace' => $attributes['birthplace'],
                'date_of_birth' => $attributes['date_of_birth'],
                'weight' => $attributes['weight'],
                'rating' => $attributes['rating'],
                'about' => $attributes['about'],
                'federation_id' => $attributes['federation_id'],
                'image' => $image_path . '/' . $image_filename,
            ]);

            return redirect(route('admin.athletes', [$federation->site]));
        }

        return redirect()->back();
    }

    public function update(Federation $federation, Athlete $athlete)
    {
        $attributes = $this->validateAthleteInfo(\request());

        $athlete->update($attributes);

        return redirect(route('admin.athlete.show', [$federation->site, $athlete->id]));
    }

    public function destroy(Federation $federation, Athlete $athlete)
    {
        $athlete->delete();

        return redirect(route('admin.athletes', [$federation->site]));
    }

    public function validateAthleteInfo($data){
        return request()->validate([
            'fullname' => 'required',
            'date_of_birth' => 'required',
            'birthplace' => 'required',
            'weight' => 'required',
            'rating' => 'required',
            'federation_id' => 'required',
            'about' => 'required',
        ]);
    }
}
